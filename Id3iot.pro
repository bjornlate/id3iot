APP_NAME = Id3iot

CONFIG += qt warn_on cascades10

LIBS += -lbbsystem -lbbdevice -lbb -lbbdata 
LIBS += -lbbmultimedia
LIBS += -L../../taglib/taglib -ltag
LIBS += -lbbcascadespickers
LIBS += -lbbplatform

INCLUDEPATH += ../src
INCLUDEPATH += ../src/id3iot
INCLUDEPATH += ../../taglib \
../../taglib/taglib \
../../taglib/taglib/ape \
../../taglib/taglib/asf \
../../taglib/taglib/ape \
../../taglib/taglib/flac \ 
../../taglib/taglib/it \
../../taglib/taglib/mod \
../../taglib/taglib/mp4 \
../../taglib/taglib/mpc \
../../taglib/taglib/mpeg \
../../taglib/taglib/mpeg \
../../taglib/taglib/mpeg/id3v2 \
../../taglib/taglib/mpeg/id3v2/frames \
../../taglib/taglib/mpeg/id3v1 \
../../taglib/taglib/mpeg/id3v1/frames \
../../taglib/taglib/ogg \
../../taglib/taglib/riff \
../../taglib/taglib/s3m \
../../taglib/taglib/toolkit \
../../taglib/taglib/trueaudio \
../../taglib/taglib/wavpak \
../../taglib/taglib/xm

include(config.pri)
