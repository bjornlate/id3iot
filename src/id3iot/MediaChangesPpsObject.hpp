/*
 * MediaChangesPpsObject.hpp
 *
 *  Created on: 2013-02-25
 *      Author: lrose
 */

#ifndef MEDIACHANGESPPSOBJECT_HPP_
#define MEDIACHANGESPPSOBJECT_HPP_

#include <QtCore>

#include <bb/PpsObject>

namespace id3iot {

class MediaChangesPpsObject : public QObject {
	Q_OBJECT

signals:
	Q_SIGNAL void fileUpdated(const QString fileName);
	Q_SIGNAL void watchListEmpty();
public:
	MediaChangesPpsObject(QObject* parent = NULL);
	virtual ~MediaChangesPpsObject();

	bool open();
	bool close();

	const QString lastError() const;

    void addToWatchList(QStringList filePaths);
	void addToWatchList(QString filePath);
	void clearWatchList();

private slots:
	void onReadyRead();

private:
	bb::PpsObject* pps;

	static const char* const PPS_PATH;

	static const char* const CHANGE;
	static const char* const CHANGE_ADDED;
    static const char* const CHANGE_DEL;
	static const char* const PATH;
	static const char* const FID;

	QStringList watchList;
};

}
#endif /* MEDIACHANGESPPSOBJECT_HPP_ */
