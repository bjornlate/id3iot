/*
 * PaymentInterface.hpp
 *
 *  Created on: Feb 16, 2014
 *      Author: sbirksted
 */

#ifndef ID3IOT_PAYMENTINTERFACE_HPP_
#define ID3IOT_PAYMENTINTERFACE_HPP_

#include <QObject>
#include <bb/platform/PaymentManager>
#include <bb/platform/ExistingPurchasesReply>
#include <bb/platform/PurchaseReply>
#include <bb/system/SystemUiResult>

namespace id3iot {

class PaymentInterface : public QObject{
    Q_OBJECT
    Q_PROPERTY(bool ownsProFeature READ ownsProFeature WRITE setOwnsProFeature NOTIFY ownsProFeatureChanged)
    Q_SIGNAL void ownsProFeatureChanged(bool);
public:

    static const char * SKU_PRO_FEATURES;

    PaymentInterface(bool testMode = false, QObject * parent = 0);
    virtual ~PaymentInterface();

    Q_INVOKABLE void setOwnsProFeature(bool b);
    Q_INVOKABLE void purchaseProFeature();
    Q_INVOKABLE void purchaseProFeatureNoDialog();
    bool ownsProFeature();

    Q_SLOT void proFeatureExplanationDialogFinished(bb::system::SystemUiResult::Type type);

    Q_SLOT void getExistingPurchases();
    Q_SLOT void onProFeaturePurchaseFinished (bb::platform::PurchaseReply *reply);

    Q_SLOT void onExistingPurchasesFinished (bb::platform::ExistingPurchasesReply *reply);

private:
    bb::platform::PaymentManager * _paymentManager;
    bool _ownsProFeature;
};
}
#endif /* ID3IOT_PAYMENTINTERFACE_HPP_ */
