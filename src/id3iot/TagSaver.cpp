/*
 * TagSaver.cpp
 *
 *  Created on: Feb 16, 2014
 *      Author: sbirksted
 */

#include <TagSaver.hpp>


#include <qdebug>

#include "fileref.h"
#include "mpegfile.h"
#include "mpeg/id3v1/id3v1genres.h"

#include "mpeg/id3v1/id3v1tag.h"

#include "mpeg/id3v2/id3v2tag.h"
#include "mpeg/id3v2/frames/attachedpictureframe.h"

#include "tpropertymap.h"
#include "tstringlist.h"
#include <QtGui/QtGui>

#include "tbytevector.h"

using TagLib::ByteVector;

using namespace id3iot;

TagSaver::TagSaver(QStringList genres, QObject * parent) : QThread(parent),
        _genres(genres),
        _isMultiSave(false)
{
}

TagSaver::~TagSaver()
{
    this->quit();
    this->wait();
}

void TagSaver::onSave(QVariantList indexPath, QVariantMap map){
    this->_indexPath = indexPath;
    this->_map = map;
    this->_isMultiSave = false;
}

void TagSaver::onMultiSave(QVariantMap filePathToIndexPath, QVariantMap map){
    this->_filePathToIndexPath = filePathToIndexPath;
    this->_map = map;
    this->_isMultiSave = true;
}

void TagSaver::run(){
    if(_isMultiSave){
        this->multiSave();
    }else{
        this->save();
    }
}


void TagSaver::save(){

    QString filePath = _map["filepath"].toString();
    qDebug() << filePath;

    bool saveSucceeded = false;
    if(!filePath.isEmpty()){
        if(saveTag(filePath, _map)){
            saveSucceeded = true;
            qDebug() << "Saved " <<filePath;
        }else{
            qCritical() << "Could not save " << filePath;
        }
    }
    emit saveFinished(saveSucceeded, _indexPath, _map);

}

void TagSaver::multiSave(){

    bool saveSucceeded = true;
    foreach(QString filePath, _filePathToIndexPath.keys()){
        saveSucceeded &= saveTag(filePath, _map);
    }
    emit multiSaveFinished(saveSucceeded, _filePathToIndexPath, _map);
}


bool TagSaver::saveTag(QString filePath, QVariantMap map){
    qDebug() << "saveTag " << QThread::currentThreadId();


    QString newCoverArtURL = map["newCoverArtURL"].toString();
    if(!newCoverArtURL.isEmpty()){
        qDebug() << "Set new cover art to " << newCoverArtURL;
        QImage image(newCoverArtURL);
        if(!image.isNull()){
            qDebug() << "we successfully loaded an image " << newCoverArtURL;
            updateCoverArt(filePath ,newCoverArtURL);
        }
    }else if(map["deleteCoverArt"].toBool()){
        qDebug() << "deleting existing cover art";
        updateCoverArt(filePath, QString());
    }


    TagLib::FileRef f(filePath.toUtf8().constData());

    if(f.isNull()){
        qCritical() << "Error creating " << filePath;
        return false;
    }

    if(map.contains("artist")){
        f.tag()->setArtist(map["artist"].toString().simplified().toUtf8().constData());
        qDebug() << "Set artist to " << map["artist"].toString();
    }
    if(map.contains("title")){
        f.tag()->setTitle(map["title"].toString().simplified().toUtf8().constData());
        qDebug() << "Set title to " << map["title"].toString();
    }
    if(map.contains("album")){
        f.tag()->setAlbum(map["album"].toString().simplified().toUtf8().constData());
        qDebug() << "Set album to " << map["album"].toString();
    }
    if(map.contains("comment")){
        f.tag()->setComment(map["comment"].toString().simplified().toUtf8().constData());
        qDebug() << "Set comment to " << map["comment"].toString();
    }

    //While we're iterating through the records, add any
    //non standard genres.
    QString customGenre = map["customGenre"].toString().simplified();
    if(!customGenre.isEmpty()){
        if(!_genres.contains(customGenre)){
            _genres.append(customGenre);
            qDebug() << "New Custom Genre added " << customGenre;
            _genres.sort();
        }
        f.tag()->setGenre(customGenre.toUtf8().constData());
        qDebug() << "Set genre to " << customGenre;
    }else if(map.contains("genre")){
        f.tag()->setGenre(map["genre"].toString().toUtf8().constData());
        qDebug() << "Set genre to " << map["genre"].toString();
    }

    bool ok = false;
    if(map.contains("year")){
        unsigned int year = map["year"].toUInt(&ok);
        if(ok){
            f.tag()->setYear(year);
            qDebug() << "Set year to " << map["year"].toString();
        }
    }
    ok = false;
    if(map.contains("track")){
        unsigned int track = map["track"].toUInt(&ok);
        if(ok){
            f.tag()->setTrack(track);
            qDebug() << "Set track to " << map["track"].toString();
        }
    }
    return f.save();
}

bool TagSaver::updateCoverArt(QString filePath, QString newCoverArtPath){
    if(filePath.endsWith(".mp3", Qt::CaseInsensitive)){
        TagLib::MPEG::File mp3(filePath.toUtf8().constData());

        qDebug() << "Has ID3v1 Tag? " << mp3.hasID3v1Tag();
        qDebug() << "Has ID3v2 Tag? " << mp3.hasID3v2Tag();

        if(!mp3.hasID3v1Tag() && !mp3.hasID3v2Tag()){
            qCritical() << "No ID3 tags to operate on!";
            return false;
        }else if(!mp3.hasID3v2Tag()){
            qDebug() << "Has ID3v1 Tag but no v2 tag. Creating v2";
            mp3.ID3v2Tag(true);
            qDebug() << "Duplicating v1 into v2";
            TagLib::Tag::duplicate(mp3.ID3v1Tag(), mp3.ID3v2Tag());
        }

        {

            TagLib::ID3v2::Tag *id3v2tag = mp3.ID3v2Tag();
            if ( id3v2tag ){
                TagLib::ID3v2::FrameList frames ;
                frames = id3v2tag->frameListMap()["APIC"];

                if (!frames.isEmpty() ){
                    for(TagLib::ID3v2::FrameList::ConstIterator it = frames.begin(); it != frames.end(); ++it){
                        TagLib::ID3v2::AttachedPictureFrame * pictureFrame = (TagLib::ID3v2::AttachedPictureFrame *)(*it) ;
                        if ( pictureFrame->type() == TagLib::ID3v2::AttachedPictureFrame::FrontCover){
                            qDebug() << "Removing existing cover art";
                            id3v2tag->removeFrame(pictureFrame);
                            break;
                        }
                    }
                }
                if(!newCoverArtPath.isEmpty()){
                    QFile newCoverArtFile(newCoverArtPath);
                    if(!newCoverArtFile.open(QIODevice::ReadOnly)){
                        qCritical() << "could not open the file " << newCoverArtFile.fileName();
                        return false;
                    }
                    QByteArray newCoverArtBytes = newCoverArtFile.readAll();
                    qDebug() << "printing the new coverart bytes size" << newCoverArtBytes.size();
                    ByteVector bv(newCoverArtBytes.constData(), (unsigned int)newCoverArtBytes.size());

                    TagLib::ID3v2::AttachedPictureFrame *pictureFrame = new TagLib::ID3v2::AttachedPictureFrame();
                    pictureFrame->setType(TagLib::ID3v2::AttachedPictureFrame::FrontCover);
                    if(newCoverArtPath.toLower().endsWith(".png")){
                        pictureFrame->setMimeType("image/png");
                    }else{
                        pictureFrame->setMimeType("image/jpeg");
                    }
                    pictureFrame->setPicture(bv);
                    id3v2tag->addFrame(pictureFrame);

                    if(mp3.save()){
                       qDebug() << "saved the tag successfully";
                       return true;
                    }else{
                        qCritical() << "couldn't save the tag";
                    }
                }
//apic = TagLib::ID3v2::AttachedPictureFrame.new
//  apic.mime_type = "image/jpeg"
//  apic.description = "Cover"
//  apic.type = TagLib::ID3v2::AttachedPictureFrame::FrontCover
//  apic.picture = File.open("cover.jpg", 'rb') { |f| f.read }
            }
        }
    }
    return false;
}

QByteArray TagSaver::getCoverArt(QString filePath){
    //Set the artwork from the id3 tag
    if(filePath.endsWith(".mp3", Qt::CaseInsensitive)){
        TagLib::MPEG::File mp3(filePath.toUtf8().constData());
        qDebug() << "Has ID3v1 Tag? " << mp3.hasID3v1Tag();
        qDebug() << "Has ID3v2 Tag? " << mp3.hasID3v2Tag();
        if(mp3.hasID3v2Tag()){

            TagLib::ID3v2::Tag *id3v2tag = mp3.ID3v2Tag();
            if ( id3v2tag ){
                TagLib::ID3v2::FrameList frames ;
                frames = id3v2tag->frameListMap()["APIC"];

                if (!frames.isEmpty() ){
                    for(TagLib::ID3v2::FrameList::ConstIterator it = frames.begin(); it != frames.end(); ++it){
                        TagLib::ID3v2::AttachedPictureFrame * pictureFrame = (TagLib::ID3v2::AttachedPictureFrame *)(*it) ;
                        if ( pictureFrame->type() == TagLib::ID3v2::AttachedPictureFrame::FrontCover){
                            QByteArray imageBytes(pictureFrame->picture().data(), pictureFrame->picture().size());
                            return imageBytes;
                        }
                    }
                }
            }
        }
    }
    return QByteArray();
}
