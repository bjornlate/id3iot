/*
 * Utils.cpp
 *
 *  Created on: Mar 18, 2014
 *      Author: sbirksted
 */

#include <Utils.hpp>
#include <QDebug>

using namespace id3iot;

Utils::Utils(QObject * parent) : QObject(parent)
{

}

Utils::~Utils(){
}

QString Utils::cycleEncoding(QString src){
    qDebug() << "cycleEncoding " << src;
    qDebug() << "cycleEncoding latin1 " << src.toLatin1();
    qDebug() << "cycleEncoding utf8 " << src.toUtf8();
    qDebug() << "cycleEncoding ascii " << src.toAscii();

    return src.toLatin1();
}
