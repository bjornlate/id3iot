/*
 * MediaChangesPpsObject.cpp
 *
 *  Created on: 2013-02-25
 *      Author: lrose
 */

#include "MediaChangesPpsObject.hpp"

namespace id3iot {

const char* const MediaChangesPpsObject::PPS_PATH = "/pps/services/multimedia/sync/changes?delta";

const char* const MediaChangesPpsObject::CHANGE = "change";
const char* const MediaChangesPpsObject::CHANGE_ADDED = "added";
const char* const MediaChangesPpsObject::CHANGE_DEL = "del";
const char* const MediaChangesPpsObject::PATH = "path";
const char* const MediaChangesPpsObject::FID = "fid";

MediaChangesPpsObject::MediaChangesPpsObject(QObject* parent) : QObject(parent), pps(new bb::PpsObject(PPS_PATH, this)) {
	pps->setReadyReadEnabled(true);

	connect(pps, SIGNAL(readyRead()), SLOT(onReadyRead()));
}

MediaChangesPpsObject::~MediaChangesPpsObject() {
	pps->close();
}

bool MediaChangesPpsObject::open() {
	return pps->open(bb::PpsOpenMode::Subscribe);
}

bool MediaChangesPpsObject::close() {
	return pps->close();
}

const QString MediaChangesPpsObject::lastError() const {
    return pps->errorString();
}

void MediaChangesPpsObject::onReadyRead() {
	bool ok(false);

	QByteArray bytes(pps->read(&ok));

	qDebug() << "onReadyRead: " << bytes;

	QVariantMap map(pps->decode(bytes, &ok));

	if (!ok) {
		qWarning("MediaChangesPpsObject::onReadyRead() - unable to parse data from PPS object! errorMsg=%s", qPrintable(pps->errorString()));
		return;
	}

	map = map.begin().value().toMap();

	if (map.contains(CHANGE) && (map.value(CHANGE) == CHANGE_ADDED || map.value(CHANGE) == CHANGE_DEL)) {
		QString path(map.value(PATH).toString());
		qDebug() << "onReadyRead change: " << path;

		foreach(QString watched, watchList){
		    if(watched.contains(path)){
                qDebug() << "change in watchlist: " << path;

                emit fileUpdated(watched);
                watchList.removeOne(watched);

                if(watchList.isEmpty()){
                    qDebug() << "watchlist empty";
                    emit watchListEmpty();
                }
		    }
		}
	}
}

void MediaChangesPpsObject::addToWatchList(QStringList filePaths){
    qDebug() << "appending list to watch list: " << filePaths;
    watchList.append(filePaths);
}


void MediaChangesPpsObject::addToWatchList(QString filePath){
	qDebug() << "appending to watch list: " << filePath;
	watchList.append(filePath);
}

void MediaChangesPpsObject::clearWatchList(){
	watchList.clear();
}

} /* namespace id3iot */
