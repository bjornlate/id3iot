/*
 * Utils.hpp
 *
 *  Created on: Mar 18, 2014
 *      Author: sbirksted
 */

#ifndef ID3IOT_UTILS_HPP_
#define ID3IOT_UTILS_HPP_
#include <QObject>

namespace id3iot{

class Utils: public QObject{
Q_OBJECT
public:
    Utils(QObject * parent = 0);
    virtual ~Utils();

    Q_INVOKABLE QString cycleEncoding(QString src);

};

} /* namespace id3iot */

#endif /* ID3IOT_UTILS_HPP_ */
