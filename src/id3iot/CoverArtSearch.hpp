/*
 * CoverArtSearch.hpp
 *
 *  Created on: Mar 11, 2014
 *      Author: sbirksted
 */

#ifndef COVERARTSEARCH_HPP_
#define COVERARTSEARCH_HPP_

#include <QObject>
#include <QVariantMap>
namespace bb {
    namespace cascades {
        class NavigationPane;
    }
}

namespace id3iot{

class CoverArtSearch: public QObject{
    Q_OBJECT
    Q_PROPERTY(QString imagePath READ imagePath NOTIFY imagePathChanged)
    Q_SIGNAL void imagePathChanged(QString);
public:
    CoverArtSearch(QObject * parent = 0);
    virtual ~CoverArtSearch();

    Q_INVOKABLE void search(QString artist, QString album);
    Q_INVOKABLE QString imagePath();
private:
    Q_SLOT void onSelectImage(const QString& path);
    QString _imagePath;
    bb::cascades::NavigationPane * _navPane;
};

} /* namespace id3iot */

#endif /* COVERARTSEARCH_HPP_ */
