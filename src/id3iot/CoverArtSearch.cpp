/*
 * CoverArtSearch.cpp
 *
 *  Created on: Mar 11, 2014
 *      Author: sbirksted
 */

#include <bb/system/InvokeManager.hpp>
#include <CoverArtSearch.hpp>
#include <bb/Application>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/Page>
#include <bb/cascades/QmlDocument>
#include <bb/system/InvokeRequest>
#include <bb/system/SystemToast>

using bb::Application;
using bb::cascades::NavigationPane;
using bb::cascades::Page;
using bb::cascades::QmlDocument;
using bb::system::InvokeManager;
using bb::system::InvokeRequest;

namespace id3iot{

CoverArtSearch::CoverArtSearch(QObject * parent) : QObject(parent){

}

CoverArtSearch::~CoverArtSearch(){
}

void CoverArtSearch::search(QString artist, QString album){

//
//    QUrl url(searchQuery.simplified().replace(' ','+'));
//    qDebug() << url;
//    InvokeManager invokeManager;
//
//    InvokeRequest request;
//    request.setUri(url);
//    request.setTarget("sys.browser");
//
//    invokeManager.invoke(request);
    QmlDocument * qml = QmlDocument::create("asset:///ImageSearchPage.qml").parent(this);
    if(!qml){
        qCritical() << "QmlDocument could not be created for ImageSearchPage";
        return;
    }
    Page * imageSearchPage = qml->createRootObject<Page>();
    if(!imageSearchPage){
        qCritical() << "Sheet could not be created from QmlDocument for ImageSearchPage";
        return;
    }
    connect(imageSearchPage, SIGNAL(selectImage(const QString&)), this, SLOT(onSelectImage(const QString&)));

    _navPane = Application::instance()->findChild<NavigationPane*>("navPane");
    if(!_navPane){
        qCritical() << "NavPane couldn't be found";
    }
    _navPane->push(imageSearchPage);

    //QVariantMap map = tagMap.toMap();
    QString searchQuery;
    //searchQuery.append(map["artist"].toString());
    searchQuery.append(artist);
    searchQuery.append(' ');
    //searchQuery.append(map["album"].toString());
    searchQuery.append(album);
    searchQuery.append(' ');
    searchQuery.append("album cover");

    imageSearchPage->setProperty("initialSearchQuery", searchQuery);
}

void CoverArtSearch::onSelectImage(const QString& path)
{
    qDebug() << "onSelect" << path;
    _navPane->pop();

    bb::system::SystemToast *toast = new bb::system::SystemToast(this);
    toast->setPosition(bb::system::SystemUiPosition::MiddleCenter);
    toast->setBody("Image saved to your pictures.");
    toast->show();

    _imagePath = path;
    emit imagePathChanged(_imagePath);
}

QString CoverArtSearch::imagePath(){
    return _imagePath;
}

} /* namespace id3iot */
