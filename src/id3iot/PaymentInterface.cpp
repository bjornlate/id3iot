/*
 * PaymentInterface.cpp
 *
 *  Created on: Feb 16, 2014
 *      Author: sbirksted
 */

#include <PaymentInterface.hpp>
#include <bb/system/SystemToast>
#include <bb/system/SystemDialog>

using namespace bb::platform;
using namespace bb::system;
using namespace id3iot;

const char * PaymentInterface::SKU_PRO_FEATURES = "id3iot_pro_features";

PaymentInterface::PaymentInterface(bool testMode, QObject * parent) : QObject(parent),
        _ownsProFeature(false)
{
 _paymentManager = new PaymentManager(this);
 if(testMode){
     _paymentManager->setConnectionMode(PaymentConnectionMode::Test);
 }
}

PaymentInterface::~PaymentInterface()
{
}

bool PaymentInterface::ownsProFeature(){
    return _ownsProFeature;
}

void PaymentInterface::setOwnsProFeature(bool b){
    _ownsProFeature = b;
    emit ownsProFeatureChanged(_ownsProFeature);
}


void PaymentInterface::getExistingPurchases(){
    connect(_paymentManager, SIGNAL(existingPurchasesFinished (bb::platform::ExistingPurchasesReply *)), this, SLOT(onExistingPurchasesFinished (bb::platform::ExistingPurchasesReply *)));
    _paymentManager->requestExistingPurchases(true);
}


void PaymentInterface::onExistingPurchasesFinished (bb::platform::ExistingPurchasesReply *reply){
    disconnect(_paymentManager, SIGNAL(existingPurchasesFinished (bb::platform::ExistingPurchasesReply *)), this, SLOT(onExistingPurchasesFinished (bb::platform::ExistingPurchasesReply *)));

    if(reply->errorCode() == 0){
        QList<PurchaseReceipt> purchases = reply->purchases();
        foreach(PurchaseReceipt pr, purchases){
            if(pr.digitalGoodSku() == SKU_PRO_FEATURES){
                qCritical() << "onExistingPurchasesFinished, pro feature found";
                setOwnsProFeature(true);
            }
        }
    }else{
        qCritical() << "onExistingPurchasesFinished reply error " << reply->errorText();
    }
    reply->deleteLater();
}

void PaymentInterface::purchaseProFeature(){

    SystemDialog * dialog = new SystemDialog(this);
    dialog->setTitle(tr("Upgrade to IDIOT Pro"));
    dialog->setBody(tr("To perform this action, you need to purchase the upgrade to ID3IOT Pro.\n\nWith ID3IOT Pro you can edit multiple tags at a time and also edit Cover Art!"));

    //dialog->cancelButton()->setLabel(QString());
    //dialog->confirmButton()->setLabel("Ok");
    connect(dialog, SIGNAL(finished(bb::system::SystemUiResult::Type)), this, SLOT(proFeatureExplanationDialogFinished(bb::system::SystemUiResult::Type)));
    dialog->show();
}

void PaymentInterface::purchaseProFeatureNoDialog(){
    connect(_paymentManager, SIGNAL(purchaseFinished (bb::platform::PurchaseReply *)),this, SLOT(onProFeaturePurchaseFinished (bb::platform::PurchaseReply *)));
    _paymentManager->requestPurchase(QString(), SKU_PRO_FEATURES);
}

void PaymentInterface::proFeatureExplanationDialogFinished(bb::system::SystemUiResult::Type type){

    if(type == SystemUiResult::ConfirmButtonSelection){
        connect(_paymentManager, SIGNAL(purchaseFinished (bb::platform::PurchaseReply *)),this, SLOT(onProFeaturePurchaseFinished (bb::platform::PurchaseReply *)));
        _paymentManager->requestPurchase(QString(), SKU_PRO_FEATURES);
    }
    sender()->deleteLater();
}

void PaymentInterface::onProFeaturePurchaseFinished (bb::platform::PurchaseReply *reply){
    disconnect(_paymentManager, SIGNAL(purchaseFinished (bb::platform::PurchaseReply *)),this, SLOT(onProFeaturePurchaseFinished (bb::platform::PurchaseReply *)));

    if(reply->isError() || !reply->receipt().isValid()){
        qCritical() << "Error purchasing " << reply->digitalGoodSku();
        SystemToast * toast = new SystemToast(this);
        connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
        toast->button()->setLabel(QString(tr("OK")));
        toast->setBody(tr("The audio tag changes were discarded because Pro feature was not purchased"));
        toast->show();
    }else{
        setOwnsProFeature(true);
    }
    reply->deleteLater();
}

