/*
 * TagSaver.hpp
 *
 *  Created on: Feb 16, 2014
 *      Author: sbirksted
 */

#ifndef TAGSAVER_HPP_
#define TAGSAVER_HPP_

#include <QObject>
#include <QVariantMap>
#include <QStringList>
#include <QThread>

namespace id3iot {
class TagSaver: public QThread
{
    Q_OBJECT
    Q_SIGNAL void saveFinished(bool, QVariantList, QVariantMap);
    Q_SIGNAL void multiSaveFinished(bool, QVariantMap, QVariantMap);
public:
    TagSaver(QStringList genres, QObject * parent = 0);
    virtual ~TagSaver();

    Q_INVOKABLE void onSave(QVariantList indexPath, QVariantMap map);
    Q_INVOKABLE void onMultiSave(QVariantMap filePathToindexPath, QVariantMap map);
    static QByteArray getCoverArt(QString filePath);
protected:
    virtual void run();
    void save();
    void multiSave();

private:
    bool saveTag(QString filePath, QVariantMap map);
    bool updateCoverArt(QString filePath, QString newCoverArtPath);

    QStringList _genres;

    QVariantMap _map;
    QVariantMap _filePathToIndexPath;
    QVariantList _indexPath;
    bool _isMultiSave;
};
}
#endif /* TAGSAVER_HPP_ */
