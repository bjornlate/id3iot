#include "Id3TagApplicationUI.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/data/SqlDataAccess>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/Dialog>

#include <bb/cascades/ListView>
#include <bb/cascades/databinding/groupdatamodel.h>
#include <bb/cascades/controls/textfield.h>
#include <bb/system/InvokeManager.hpp>
#include <bb/cascades/controls/tabbedpane.h>
#include <bb/cascades/controls/sheet.h>
#include <bb/cascades/Page>
#include <bb/cascades/ImageView>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/TitleBar>
#include <bb/cascades/ActionItem>
#include <bb/cascades/Container>
#include <bb/cascades/Label>
#include <bb/multimedia/MediaPlayer>
#include <bb/cascades/Menu>
#include <bb/cascades/HelpActionItem>
#include <bb/cascades/SettingsActionItem>
#include <bb/system/SystemToast>
#include <bb/cascades/SceneCover>
#include <bb/cascades/HelpActionItem>
#include "SearchableGroupDataModel.h"
#include <QtCore/QVariantMap>
#include <bb/cascades/controls/dropdown.h>
#include <TagSaver.hpp>
#include <Utils.hpp>

#include "fileref.h"
#include "mpegfile.h"
#include "mpeg/id3v1/id3v1genres.h"

#include "mpeg/id3v1/id3v1tag.h"

#include "mpeg/id3v2/id3v2tag.h"
#include "mpeg/id3v2/frames/attachedpictureframe.h"

#include "tpropertymap.h"
#include <bb/system/CardDoneMessage>
#include "tstringlist.h"
#include "PictureEditorLauncher.hpp"

using namespace bb::cascades;
using namespace bb::data;
using namespace bb::multimedia;
using namespace bb::system;
using namespace TagLib::ID3v1;
using namespace TagLib;
using namespace id3iot;

const char * Id3TagApplicationUI::HAS_PRO_FEATURE = "hasProUpgrade";

Id3TagApplicationUI::Id3TagApplicationUI(bb::cascades::Application *app) :
        QObject(app),
        _invokeManager(new InvokeManager(this)),
        _currentDataModel(0),
        _paymentInterface(new PaymentInterface(true, this)),
        noArtistDataModel(0),
        noAlbumDataModel(0),
        allSongsDataModel(0),
        genresDataModel(0),
        noCoverArtDataModel(0),
        _savingDialog(0)
{


	connect(_invokeManager, SIGNAL(invoked(const bb::system::InvokeRequest &)), this, SLOT(onInvoked(const bb::system::InvokeRequest &)));


    // prepare the localization
    translator = new QTranslator(this);
    localeHandler = new LocaleHandler(this);
    if(!QObject::connect(localeHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()))) {
        // This is an abnormal situation! Something went wrong!
        // Add own code to recover here
        qWarning() << "Recovering from a failed connect()";
    }
    // initial load
    onSystemLanguageChanged();

    _settings = new QSettings("data/id3iot-settings.conf", QSettings::NativeFormat, this);
    bb::cascades::QmlDocument::defaultDeclarativeEngine()->rootContext()->setContextProperty("PaymentInterface", _paymentInterface);


    connect(_paymentInterface, SIGNAL(ownsProFeatureChanged(bool)), this, SLOT(onOwnsProFeatureChanged(bool)));
    /**
     * Init the setting for Pro Feature
     */
    if(!_settings->contains(HAS_PRO_FEATURE)){
        qCritical () << "settings doesn't contain the pro feature value. setting it to false";
        _settings->setValue(HAS_PRO_FEATURE, false);
    }else{
        bool ownsProFeature = _settings->value(HAS_PRO_FEATURE, false).toBool();
        qCritical () << "settings contains the pro feature value: " << ownsProFeature;
        _paymentInterface->setOwnsProFeature(ownsProFeature);
    }

    /**
     * if its not owned, hook the signal to update the settings, and fetch the existing purchases.
     */
    if(!_paymentInterface->ownsProFeature()){
        qCritical() << "No purchase for pro feature discovered, fetching purchases from the server...";
        _paymentInterface->getExistingPurchases();
    }

    bb::cascades::QmlDocument::defaultDeclarativeEngine()->rootContext()->setContextProperty("Utils", new Utils(this));


    query = "SELECT DISTINCT title, res.fid as fid, t.imgfs_filename, mountpath, basepath, \
        res.filename as filename, artist, album, \
        track, disc, year, compilation, res.extension as extension, \
        duration, aa.imgfs_filename as coverart, t.imgfs_filename as thumbnail, \
        res.album_id, res.artist_id, g.genre as genre \
        FROM (((((((audio_metadata am INNER JOIN files f ON am.fid = f.fid) res \
                  INNER JOIN folders fo ON res.folderid = fo.folderid) \
                  LEFT OUTER JOIN audio_artworks aa ON res.artwork_id = aa.artwork_id) \
                  LEFT OUTER JOIN thumbnails t ON aa.thumbid = t.thumbid) \
                  LEFT OUTER JOIN genres g ON g.genre_id = res.genre_id) \
                  LEFT OUTER JOIN mediastore_metadata) \
                  LEFT OUTER JOIN artists ar ON res.artist_id = ar.artist_id) \
                  LEFT OUTER JOIN albums al ON res.album_id = al.album_id \
             WHERE res.extension != 'amr' AND res.extension != 'wav';";

	QString genreQuery = "SELECT genre FROM genres;";
    _sda = new SqlDataAccess("db/mmlibrary.db");
    _sdaSD = new SqlDataAccess("db/mmlibrary_SD.db");

    buildGenreList();

    if(_invokeManager->startupMode() == ApplicationStartupMode::LaunchApplication){
    	initTabs();
	}

    QmlDocument *qmlCover = QmlDocument::create("asset:///SceneCover.qml").parent(this);

    if (!qmlCover->hasErrors()) {
        // Create the QML Container from using the QMLDocument.
        Container *coverContainer = qmlCover->createRootObject<Container>();

        // Create a SceneCover and set the application covere
        SceneCover *sceneCover = SceneCover::create().content(coverContainer);
        Application::instance()->setCover(sceneCover);
    }

    pps = new MediaChangesPpsObject(this);
    pps->open();
}


Id3TagApplicationUI::~Id3TagApplicationUI(){
    pps->close();
}

void Id3TagApplicationUI::onOwnsProFeatureChanged(bool proFeature){
    qCritical() << "onOwnsProFeatureChanged " << proFeature;
    if(proFeature && Application::instance()->menu() && _proFeatureActionItem){
        Application::instance()->menu()->removeAction(_proFeatureActionItem);
    }
    _settings->setValue(HAS_PRO_FEATURE, proFeature);
}

void Id3TagApplicationUI::initTabs(){


    fetchMMSyncRecords(_sda);
    fetchMMSyncRecords(_sdaSD);

    TabbedPane * tabs = TabbedPane::create();
    // Set created root object as the application scene


    QVariantMap conditionsMap;
    conditionsMap["album"] = "";

    AbstractPane * noAlbumPage = createPage(filterRecords(conditionsMap, _mmSyncRecords), QStringList() << "album" << "track");
    tabs->add(Tab::create().title(tr("No Album")).imageSource(QUrl("asset:///images/ic_unknown_album.png"))
    		.content(noAlbumPage));
    ListView * listView = noAlbumPage->findChild<ListView*>("listView");
    if(listView){
        noAlbumDataModel = qobject_cast<SearchableGroupDataModel*>(listView->dataModel());
    }
    TextField * searchField = noAlbumPage->findChild<TextField*>("searchField");
    if(searchField){
        connect(this, SIGNAL(resetSearch()), searchField, SLOT(resetText()));
        searchField = 0;
    }

    conditionsMap.clear();
    conditionsMap["artist"] = "";
    AbstractPane * noArtistPage = createPage(filterRecords(conditionsMap, _mmSyncRecords), QStringList() << "artist" << "track");
    tabs->add(Tab::create().title(tr("No Artist")).imageSource(QUrl("asset:///images/ic_unknown_artist.png"))
    		.content(noArtistPage));
    listView = noArtistPage->findChild<ListView*>("listView");
    if(listView){
        noArtistDataModel = qobject_cast<SearchableGroupDataModel*>(listView->dataModel());
    }
    searchField = noArtistPage->findChild<TextField*>("searchField");
    if(searchField){
        connect(this, SIGNAL(resetSearch()), searchField, SLOT(resetText()));
        searchField = 0;
    }

    conditionsMap.clear();
    conditionsMap["coverart"] = "";
    AbstractPane * noCoverArtPage = createPage(filterRecords(conditionsMap, _mmSyncRecords), QStringList() << "album" << "track");
    tabs->add(Tab::create().title(tr("No Cover Art")).imageSource(QUrl("asset:///images/ic_unknown_coverart.png"))
            .content(noCoverArtPage));
    listView = noCoverArtPage->findChild<ListView*>("listView");
    if(listView){
        noCoverArtDataModel = qobject_cast<SearchableGroupDataModel*>(listView->dataModel());
    }
    searchField = noCoverArtPage->findChild<TextField*>("searchField");
    if(searchField){
        connect(this, SIGNAL(resetSearch()), searchField, SLOT(resetText()));
        searchField = 0;
    }



    AbstractPane * allSongsPage = createPage(_mmSyncRecords, QStringList() << "album" << "track");
    tabs->add(Tab::create().title(tr("All Songs")).imageSource(QUrl("asset:///images/ic_genres.png"))
    		.content(allSongsPage));
    allSongsPage->setProperty("showSorting", true);
    listView = allSongsPage->findChild<ListView*>("listView");
    if(listView){
        allSongsDataModel = qobject_cast<SearchableGroupDataModel*>(listView->dataModel());
    }
    searchField = allSongsPage->findChild<TextField*>("searchField");
    if(searchField){
        connect(this, SIGNAL(resetSearch()), searchField, SLOT(resetText()));
        searchField = 0;
    }

//    AbstractPane * genresPage = createPage(_mmSyncRecords, QStringList() << "genre" << "album" << "track");
//    tabs->add(Tab::create().title(tr("By Genre")).imageSource(QUrl("asset:///images/ic_genres.png"))
//            .content(genresPage));
//    listView = genresPage->findChild<ListView*>("listView");
//    if(listView){
//        genresDataModel = qobject_cast<SearchableGroupDataModel*>(listView->dataModel());
//    }
//    searchField = genresPage->findChild<TextField*>("searchField");
//    if(searchField){
//        connect(this, SIGNAL(resetSearch()), searchField, SLOT(resetText()));
//        searchField = 0;
//    }


    tabs->setShowTabsOnActionBar(true);
    Application::instance()->setScene(tabs);



    Menu * menu = new Menu;
    HelpActionItem * helpAction = new HelpActionItem();
    menu->setHelpAction(helpAction);
    Application::instance()->setMenu(menu);
    connect(helpAction, SIGNAL(triggered()), this, SLOT(openHelpSheet()));

    ActionItem * refreshAction = ActionItem::create().imageSource(QUrl("asset:///images/ic_refresh.png")).title(tr("Refresh"));
    connect(refreshAction, SIGNAL(triggered()), this, SLOT(refreshDataModels()));
    menu->addAction(refreshAction);

    if(!_paymentInterface->ownsProFeature()){
       _proFeatureActionItem = ActionItem::create().title(tr("Pro Upgrade")).imageSource(QUrl("asset:///images/ic_like.png"));
       menu->addAction(_proFeatureActionItem);
       connect(_proFeatureActionItem, SIGNAL(triggered()), _paymentInterface, SLOT(purchaseProFeatureNoDialog()));
    }

}

void Id3TagApplicationUI::onInvoked(const bb::system::InvokeRequest &request){
	Q_UNUSED(request);
	if(_invokeManager->startupMode() == ApplicationStartupMode::InvokeCard){
		qDebug() << "Invoked as a card";


		QVariantMap map;
		QString filePath = request.uri().toString();
        map["filepath"] = filePath.remove("file://");
        map["filename"] = filePath.right(filePath.length() - filePath.lastIndexOf("/") - 1);


		qDebug() << "data" << request.data();
		qDebug() << "uri" << map["filepath"].toString();
		qDebug() << "filename" << map["filename"].toString();

		Page * page = createId3EditPage(QVariantList(), map);

		/**
		 * Fetch the cover art from the ID3 tag directly.
		 * TODO: optimize this so we don't have to open the file twice to get the
		 * ID3 tag info :/
		 */
		QByteArray imageBytes = TagSaver::getCoverArt(filePath);
		ImageView * img = page->findChild<ImageView*>("coverArt");
        if(imageBytes.size() > 0 && img){
            img->setImage(Image(imageBytes));
        }


		bool cardShown = false;

        if(page){
            qWarning() << "Page ok";

            if(page->titleBar()){
                qDebug() << "connecting";
                connect(page->titleBar()->dismissAction(), SIGNAL(triggered()), this, SLOT(onCardDone()));
                Application::instance()->setScene(page);
                cardShown = true;
            }
        }
		if(!cardShown){
			qCritical() << "Card failure";
			onCardDone();
		}
	}else{
		qDebug() << "Invoked in other means";
	}
}

void Id3TagApplicationUI::onCardDone(){
	// Assemble response message
	CardDoneMessage message;
	message.setData(tr("Card: I am done. yay!"));
	message.setDataType("text/plain");
	message.setReason(tr("Success!"));

	// Send message
	_invokeManager->sendCardDone(message);
}

void Id3TagApplicationUI::onSystemLanguageChanged()
{
    QCoreApplication::instance()->removeTranslator(translator);
    // Initiate, load and install the application translation files.
    QString locale_string = QLocale().name();
    QString file_name = QString("Id3iot_%1").arg(locale_string);
    qDebug() << "using language file: " + file_name;
    if (translator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(translator);
    }else{
        qCritical() << "Couldn't load a translation";
    }
}

void Id3TagApplicationUI::playSong(QString fid, bool preview){
	qDebug() << "playSong" << fid;

	InvokeRequest request;
	if(preview){
        request.setUri("file://"+fid);
        //request.setMimeType("audio/mp3");
        request.setTarget("sys.mediaplayer.previewer");
        request.setAction("bb.action.VIEW");
	}else{
	    //TODO: sort out this invoke, its not working correctly.
        request.setMimeType("audio/mp3");
        request.setUri("music://albums?play="+fid); //although this isn't *officially* supported, it works. Emmanuel confirmed that the album_id param is ignored when passed!
        request.setTarget("sys.music.app");
	}
	_invokeManager->invoke(request);
}

QVariantList Id3TagApplicationUI::filterRecords(QVariantMap conditions, QVariantList source){

	QVariantList list;
	foreach(QVariant record, source){
		QVariantMap map = record.toMap();
		foreach(QString key, conditions.keys()){
			if(map[key] == conditions[key]){
				list << map;
			}
		}

		//While we're iterating through the records, add any
		//non standard genres.
		QString genre = map["genre"].toString().simplified();
		if(!genre.isEmpty() && !_genres.contains(genre)){
			_genres.append(genre);
		}

	}
	return list;
}

bb::cascades::AbstractPane * Id3TagApplicationUI::createPage(QVariantList records, QStringList sortingKeys){
    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    // Create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();


	// Create a ListView control and add the model to the list
    ListView *listView = root->findChild<ListView*>("listView");
    if(listView){
    	// Create a data model with sorting keys for firstname and lastname
    	GroupDataModel *model = new GroupDataModel(sortingKeys);

    	model->setGrouping(ItemGrouping::ByFullValue);
    	model->insertList(records);

    	QStringList searchProperties;
    	searchProperties << "title" << "artist" << "album" << "genre";
    	SearchableGroupDataModel * sgdm = new SearchableGroupDataModel(model, searchProperties, listView);
    	listView->setDataModel(sgdm);

    	connect(this, SIGNAL(clearMMsyncRecords()), sgdm, SLOT(clear()));
    	connect(this, SIGNAL(updateMMsyncRecords(const QVariantList &)), sgdm, SLOT(insertList(const QVariantList &)));

    	connect(this, SIGNAL(findAndUpdate(const QVariantMap &, const QVariantMap &)), sgdm, SLOT(findAndUpdate(const QVariantMap &, const QVariantMap &)));

    	connect(listView, SIGNAL(playSong(QString, bool)), this, SLOT(playSong(QString, bool)));
    	connect(listView, SIGNAL(triggered(QVariantList)), this, SLOT(onListTriggered(QVariantList)));
    	connect(listView, SIGNAL(editMultiple(QVariant)), this, SLOT(openMultipleEditPage(QVariant)));

    	TextField * searchText = root->findChild<TextField*>("searchField");
    	if(searchText){
    		connect(searchText, SIGNAL(textChanging(const QString)), sgdm, SLOT(setSearchQuery(QString)));
    	}
    }

    return root;
}

void Id3TagApplicationUI::refreshDataModels(){

    disconnect(pps, SIGNAL(watchListEmpty()), this, SLOT(refreshDataModels()));
    emit resetSearch();

    qDebug() << "refreshDataModels "<< QThread::currentThreadId();
    _mmSyncRecords.clear();
    fetchMMSyncRecords(_sdaSD);
    fetchMMSyncRecords(_sda);

    QVariantMap conditionsMap;

    if(noAlbumDataModel){
        conditionsMap["album"] = "";
        noAlbumDataModel->clear();
        noAlbumDataModel->sourceDataModel()->insertList(filterRecords(conditionsMap, _mmSyncRecords));
    }

    if(noArtistDataModel){
        conditionsMap.clear();
        conditionsMap["artist"] = "";
        noArtistDataModel->clear();
        noArtistDataModel->sourceDataModel()->insertList(filterRecords(conditionsMap, _mmSyncRecords));
    }

    if(noCoverArtDataModel){
        conditionsMap.clear();
        conditionsMap["coverart"] = "";
        noCoverArtDataModel->clear();
        noCoverArtDataModel->sourceDataModel()->insertList(filterRecords(conditionsMap, _mmSyncRecords));
    }

    if(allSongsDataModel){
        allSongsDataModel->clear();
        allSongsDataModel->sourceDataModel()->insertList(_mmSyncRecords);
    }

    if(genresDataModel){
        genresDataModel->clear();
        genresDataModel->sourceDataModel()->insertList(_mmSyncRecords);
    }


    if(_currentDataModel && !matchMap.isEmpty()){
        QVariantList newIndexPath = _currentDataModel->findExact(matchMap);
        if(newIndexPath.size() >0){
            ListView * listView = qobject_cast<ListView*>(_currentDataModel->parent());
            if(listView){
                listView->scrollToItem(newIndexPath);
            }
            showUpdatedTagsToast();
        }else{
            SystemToast * toast = new SystemToast(this);
            connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
            toast->setBody(tr("Tag(s) are updated, but are no longer in this view."));
            toast->show();
        }
    }
    matchMap.clear();
    _currentDataModel = 0;

}

void Id3TagApplicationUI::onListTriggered(QVariantList indexPath){
	if(indexPath.length() == 2){
		ListView * listView = qobject_cast<ListView*>(sender());
		if(listView){
			if(listView->dataModel()){
				_currentDataModel = qobject_cast<SearchableGroupDataModel*>(listView->dataModel());
				QVariant data = _currentDataModel->data(indexPath);
				QVariantMap map = data.toMap();

				openId3EditSheet(indexPath, map);
			}
		}
	}
}

void Id3TagApplicationUI::openMultipleEditPage(QVariant indexPaths){
	Sheet * sheet = Sheet::create().parent(this);
	if(!sheet){
		return;
	}
	QmlDocument * qml = QmlDocument::create("asset:///Id3TagMultiPage.qml").parent(this);
	if(!qml){
		return;
	}
	Page * page = qml->createRootObject<Page>();

//	Page * page = createId3EditPage(indexPath, map);
	if(!page){
		return;
	}

    NavigationPane * navPane = NavigationPane::create().objectName("navPane");
    navPane->push(page);
    sheet->setContent(navPane);

	if(page->titleBar()){
		qDebug() << "connecting";
		connect(page->titleBar()->dismissAction(), SIGNAL(triggered()), sheet, SLOT(close()));
		connect(page->titleBar()->acceptAction(), SIGNAL(triggered()), sheet, SLOT(close()));
		connect(sheet, SIGNAL(closed()), sheet, SLOT(deleteLater()));

		connect(page, SIGNAL(save(QVariant, QVariant)), this, SLOT(onMultiSave(QVariant, QVariant)));

		sheet->open();
	}

	ListView * listView = qobject_cast<ListView*>(sender());
	if(!listView){
		return;
	}

	DataModel * dataModel = listView->dataModel();
	if(!dataModel){
		return;
	}else{
        _currentDataModel = qobject_cast<SearchableGroupDataModel*>(listView->dataModel());
	}

	QVariantMap similarities;

	QVariantMap filePathToIndexPath;

	QVariantList indexPathList = indexPaths.toList();

	Container * container = page->findChild<Container*>("files");
	if(!container){
		return;
	}

	foreach(QVariant var, indexPathList){
		QVariantList indexPath = var.toList();
		if(!indexPath.isEmpty()){
			QVariantMap map = dataModel->data(indexPath).toMap();


			populateMap(map);

			bool isMp3 = true;
			QString filePath = map["filepath"].toString();
			if(!filePath.isEmpty()){

			    if(!filePath.toLower().endsWith(".mp3")){
			        isMp3 = false;
			    }

				filePathToIndexPath[filePath] = indexPath;

				container->add(Label::create(map["filename"].toString()));

			}else{
				qDebug() << "Filepath is empty?";
			}

            page->setProperty("coverArtEditable", isMp3);

			if(similarities.size() == 0){
				similarities = map;
			}else{
				if(map["album"] != similarities["album"]){
					similarities["album"] = QVariant();
				}
				if(map["artist"] != similarities["artist"]){
					similarities["artist"] = QVariant();
				}
				if(map["comment"] != similarities["comment"]){
					similarities["comments"] = QVariant();
				}
				if(map["genre"] != similarities["genre"]){
					similarities["genre"] = QVariant();
				}
				if(map["year"] != similarities["year"]){
					similarities["year"] = QVariant();
				}
				if(map["extension"] != similarities["extension"]){
				    similarities["extension"] = QVariant();
				}
			}
		}
	}
	QString genre = similarities["genre"].toString();

	//bool found = false;
	DropDown * genres = page->findChild<DropDown*>("genres");
	if(genres){
		foreach(QString name, _genres){
			Option * option = Option::create().text(name).value(name);
			genres->add(option);
			if(name.toLower() == genre.toLower()){
				//found = true;
				genres->setSelectedOption(option);
			}
		}
	}

	QString coverArtUrl = similarities["coverart"].toString();
    if(!coverArtUrl.isEmpty()){
        ImageView * img = page->findChild<ImageView*>("coverArt");
        qDebug() << "Attempting to set the cover art " << coverArtUrl;
        if(img){
            //img->setImage(Image(map["imageBytes"].toByteArray()));
            img->setImageSource(coverArtUrl);
        }
    }
//	if(!found && !genre.isEmpty()){
//		Option * option = Option::create().text(genre).value(genre);
//		genres->add(option);
//		genres->setSelectedOption(option);
//	}


	page->setProperty("tagMap", similarities);
	page->setProperty("filePathToIndexPath", filePathToIndexPath);
}


void Id3TagApplicationUI::openHelpSheet(){
	QmlDocument * qml = QmlDocument::create("asset:///HelpPage.qml").parent(this);

	if(qml->hasErrors()){
		return;
	}

	Page * page = qml->createRootObject<Page>();
	if(!page){
		return;
	}

	Sheet * sheet = Sheet::create().parent(this);
	if(!sheet){
		return;
	}
	sheet->setContent(page);

	if(page->titleBar()){
		connect(page->titleBar()->acceptAction(), SIGNAL(triggered()), sheet, SLOT(close()));
		connect(sheet, SIGNAL(closed()), sheet, SLOT(deleteLater()));

		sheet->open();
	}
}


void Id3TagApplicationUI::openId3EditSheet(QVariantList indexPath, QVariantMap map){
	Sheet * sheet = Sheet::create().parent(this);
	if(!sheet){
		return;
	}
	Page * page = createId3EditPage(indexPath, map);
	if(!page){
		return;
	}

	NavigationPane * navPane = NavigationPane::create().objectName("navPane");
	navPane->push(page);
	sheet->setContent(navPane);

	if(page->titleBar()){
		qDebug() << "connecting";
		connect(page->titleBar()->dismissAction(), SIGNAL(triggered()), sheet, SLOT(close()));
		connect(page->titleBar()->acceptAction(), SIGNAL(triggered()), sheet, SLOT(close()));
		connect(sheet, SIGNAL(closed()), sheet, SLOT(deleteLater()));

		connect(page, SIGNAL(deleteFile(QString)), sheet, SLOT(close()));
		connect(page, SIGNAL(deleteFile(QString)), this, SLOT(onDeleteFile(QString)));

		sheet->open();
	}
}

bb::cascades::Page* Id3TagApplicationUI::createId3EditPage(QVariantList indexPath, QVariantMap map){
	QmlDocument *qml = QmlDocument::create("asset:///Id3TagPage.qml").parent(this);
	if(!qml){
		return 0;
	}
	Page * page = qml->createRootObject<Page>();
	if(page){
		connect(page, SIGNAL(save(QVariant, QVariant)), this, SLOT(onSave(QVariant, QVariant)));

		page->setProperty("indexPath", indexPath);

		if(!populateMap(map)){
			return page;
		}

		QString coverArtUrl = map["coverart"].toString();
		if(!coverArtUrl.isEmpty()){
		    ImageView * img = page->findChild<ImageView*>("coverArt");
		    qDebug() << "Attempting to set the cover art " << coverArtUrl;
		    if(img){
		        //img->setImage(Image(map["imageBytes"].toByteArray()));
                img->setImageSource(coverArtUrl);
		    }
		}

		QString genre = map["genre"].toString();
		DropDown * genres = page->findChild<DropDown*>("genres");
		if(genres){
			foreach(QString name, _genres){
				Option * option = Option::create().text(name).value(name);
				genres->add(option);
				if(name.toLower() == genre.toLower()){
					genres->setSelectedOption(option);
				}
			}
		}
//		/**
//		 * If you invoke this app, the list of genres is incomplete.
//		 * TODO: Maybe they should be queried from the mmsync.dbs on launch to
//		 * make sure all are populated into the list
//		 */
//		if(!found && !genre.isEmpty()){
//			Option * option = Option::create().text(genre).value(genre);
//			genres->add(option);
//			genres->setSelectedOption(option);
//		}


		page->setProperty("tagMap", map);
		connect(page, SIGNAL(playSong(QString, bool)), this, SLOT(playSong(QString, bool)));

	}
	return page;
}

bool Id3TagApplicationUI::populateMap(QVariantMap & map){

	QString fileName = map["filename"].toString();
	QString filePath = map["filepath"].toString();

	if(filePath.isEmpty()){
		filePath = map["mountpath"].toString() + map["basepath"].toString() + fileName;
		qDebug() << filePath;
		map["filepath"] = filePath;
	}



	TagLib::FileRef f(filePath.toUtf8().constData());
	if(f.isNull()){
		qCritical("WTF the file was a failure.");
		return false;
	}

	if(!map.contains("artist")){
		map["artist"] = f.tag()->artist().toCString(true);
		qDebug() << f.tag()->artist().toCString(true);
	}
	if(!map.contains("album")){
		map["album"] = f.tag()->album().toCString(true);
		qDebug() << f.tag()->album().toCString(true);
	}
	if(!map.contains("title")){
		map["title"] = f.tag()->title().toCString(true);
		qDebug() << f.tag()->title().toCString(true);
	}
	if(!map.contains("track")){
		map["track"] = f.tag()->track();
		qDebug() << f.tag()->track();
	}
	if(!map.contains("year")){
		map["year"] = f.tag()->year();
		qDebug() << f.tag()->year();
	}
	if(!map.contains("comment")){
		map["comments"] = f.tag()->comment().toCString(true);
		qDebug() << f.tag()->comment().toCString(true);
	}
	if(!map.contains("genre")){
		map["genre"] = f.tag()->genre().toCString(true);
		qDebug() << f.tag()->genre().toCString(true);
	}

	//qDebug() << "PropertyMap: " << QString(f.audioProperties()->.toCString(true));
	map["bitrate"] = f.audioProperties()->bitrate();
	map["sampleRate"] = f.audioProperties()->sampleRate();


	if(map.value("artist").toString().isEmpty()){
		//Strip the extension off
		QString fileNameWithoutExt = fileName.left(fileName.lastIndexOf("."));
		fileNameWithoutExt = caseClean(fileNameWithoutExt);
		qDebug() << fileNameWithoutExt;

		QStringList splits = fileNameWithoutExt.simplified().split("-");
		if(splits.size() == 2){
			QString artist = splits.at(0).trimmed();
			map["artist"] = artist;
			map["title"] = splits.at(1).trimmed();
			map["album"] = "Songs by " + artist;
		}else if(splits.size() == 3){
			bool ok = false;
			int track = splits.at(0).toInt(&ok);
			if(ok){
				map["track"] = track;
				QString artist = splits.at(1).trimmed();
				map["artist"] = artist;
				map["title"] = splits.at(2).trimmed();
				map["album"] = "Songs by " + artist;
			}else{
				QString artist = splits.at(0).trimmed();
				map["artist"] = artist;
				map["title"] = splits.at(1).trimmed();
				map["album"] = "Songs by " + artist;
			}

		}else{
			QStringList spaceSplits = fileNameWithoutExt.split(" ");
			bool ok = false;
			int track = spaceSplits.at(0).toInt(&ok);
			if(ok){
				map["track"] = track;
				fileNameWithoutExt = fileNameWithoutExt.remove(spaceSplits.at(0)).trimmed();
			}

			map["artist"] = fileNameWithoutExt;
			map["album"] = fileNameWithoutExt;
		}
	}
	if(map["album"].toString().isEmpty()){
		map["album"] = "Singles";
	}
	return true;
}

QString Id3TagApplicationUI::caseClean(const QString & source){
	QString result;

	QStringList splits = source.toLower().split(" ");
	foreach(QString split, splits){
		int braceId = split.indexOf("(");
		if(braceId >= 0 && split.length() > braceId+1){
			split.replace(braceId+1, 1, split[braceId+1].toUpper());
			//split.replace("("+split.[braceId+1], "("+split[braceId+1].toUpper());
		}
		result.append(split.left(1).toUpper());
		result.append(split.right(split.length()-1));
		result.append(" ");
	}


	return result.trimmed();
}

void Id3TagApplicationUI::onSave(QVariant indexPath, QVariant var){
	QVariantMap map = var.toMap();
	TagSaver * saver = new TagSaver(_genres);


//    SystemToast * toast = new SystemToast(this);
//	connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
//	toast->setBody(tr("Updating tag..."));
//	toast->show();

    pps->addToWatchList(map["filepath"].toString());

	connect(saver, SIGNAL(saveFinished(bool, QVariantList, QVariantMap)), this, SLOT(onSaveFinished(bool, QVariantList, QVariantMap)));
	saver->onSave(indexPath.toList(),map);
	saver->start();

	showSavingDialog();
}

void Id3TagApplicationUI::onSaveFinished(bool success, QVariantList indexPath, QVariantMap map){
    Q_UNUSED(map);
    disconnect(sender(), SIGNAL(saveFinished(bool, QVariantList, QVariantMap)), this, SLOT(onSaveFinished(bool, QVariantList, QVariantMap)));

    if(!success){
        SystemToast * toast = new SystemToast(this);
        connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
        //toast->setBody(tr("Error saving %1").arg(filePath));
        toast->setBody(tr("Error saving tag"));
        toast->show();
    }

    connect(pps, SIGNAL(watchListEmpty()), this, SLOT(refreshDataModels()), Qt::UniqueConnection);
    //connect(pps, SIGNAL(watchListEmpty()), this, SLOT(showUpdatedTagsToast()), Qt::UniqueConnection);

    matchMap.clear();

    if(_currentDataModel && indexPath.size()>0){
        QVariantMap map = _currentDataModel->data(indexPath).toMap();
        matchMap["filename"] = map["filename"];
        matchMap["basepath"] = map["basepath"];
        matchMap["mountpath"] = map["mountpath"];
        qDebug() << map;
    }else{
        _currentDataModel = 0;
    }

	if(_invokeManager->startupMode() == ApplicationStartupMode::InvokeCard){
		onCardDone();
	}

	sender()->deleteLater();
}




void Id3TagApplicationUI::onMultiSave(QVariant filePathToIndexPath, QVariant map){
    qDebug() << "onMultiSave "<< QThread::currentThreadId();

	if(!_paymentInterface->ownsProFeature()){
	    qWarning() << "Prompting for purchase";
	    _paymentInterface->setProperty("fileToIndexPath", filePathToIndexPath);
	    _paymentInterface->setProperty("map", map);

	    connect(_paymentInterface, SIGNAL(ownsProFeatureChanged(bool)), this, SLOT(onProFeaturePurchasedMultiEdit(bool)), Qt::UniqueConnection);
	    _paymentInterface->purchaseProFeature();
	    return;
	}

    TagSaver * saver = new TagSaver(_genres);
    connect(saver, SIGNAL(multiSaveFinished(bool, QVariantMap, QVariantMap)), this, SLOT(onMultiSaveFinished(bool, QVariantMap, QVariantMap)), Qt::QueuedConnection);


    QVariantMap filePathToIndexPathMap = filePathToIndexPath.toMap();

    pps->addToWatchList(filePathToIndexPathMap.keys());

//    SystemToast * toast = new SystemToast(this);
//    connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
//    toast->setBody(tr("Updating tags..."));
//    toast->show();

    saver->onMultiSave(filePathToIndexPathMap,map.toMap());
    saver->start();

    showSavingDialog();
}

void Id3TagApplicationUI::onMultiSaveFinished(bool success, QVariantMap filePathToIndexPath, QVariantMap map){
    Q_UNUSED(map);
    disconnect(sender(), SIGNAL(multiSaveFinished(bool, QVariantMap, QVariantMap)), this, SLOT(onMultiSaveFinished(bool, QVariantMap, QVariantMap)));

    if(!success){
        SystemToast * toast = new SystemToast(this);
        connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
        //toast->setBody(tr("Error saving %1").arg(filePath));
        toast->setBody(tr("Error saving tags"));
        toast->show();
    }

    connect(pps, SIGNAL(watchListEmpty()), this, SLOT(refreshDataModels()), Qt::UniqueConnection);
    //connect(pps, SIGNAL(watchListEmpty()), this, SLOT(showUpdatedTagsToast()), Qt::UniqueConnection);

    QVariantList indexPath = filePathToIndexPath[filePathToIndexPath.keys().first()].toList();
    matchMap.clear();

    if(_currentDataModel && indexPath.size()>0){
        QVariantMap map = _currentDataModel->data(indexPath).toMap();
        matchMap["filename"] = map["filename"];
        matchMap["basepath"] = map["basepath"];
        matchMap["mountpath"] = map["mountpath"];
        qDebug() << map;
    }else{
        _currentDataModel = 0;
    }

    sender()->deleteLater();

}

void Id3TagApplicationUI::showUpdatedTagsToast(){
    disconnect(pps, SIGNAL(watchListEmpty()), this, SLOT(showUpdatedTagsToast()));

    SystemToast * toast = new SystemToast(this);
    connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
    toast->setBody(tr("Tag updates complete."));
    toast->show();
}

void Id3TagApplicationUI::onProFeaturePurchasedMultiEdit(bool owned){
    disconnect(_paymentInterface, SIGNAL(ownsProFeatureChanged(bool)), this, SLOT(onProFeaturePurchasedMultiEdit(bool)));
    if(owned){
        onMultiSave(_paymentInterface->property("fileToIndexPath"), _paymentInterface->property("map"));
    }
}



void Id3TagApplicationUI::fetchMMSyncRecords(bb::data::SqlDataAccess * sda){
    // Load the sql data from contacts table

	if(sda->error().errorType() == DataAccessErrorType::None){
		QVariant list = sda->execute(query);
		_mmSyncRecords.append(list.value<QVariantList>());
		// Add the data to the model
	}else{
		qDebug() << sda->error().errorMessage();
	}

}

void Id3TagApplicationUI::buildGenreList(){
    for(unsigned int i=0;i<genreList().size();++i){
        _genres.append(genreList()[i].toCString(true));
    }


    fetchGenres(_sdaSD);
    fetchGenres(_sda);
    _genres.sort();
}

void Id3TagApplicationUI::fetchGenres(SqlDataAccess * _sda){
//    SqlDataAccess * _sda = new SqlDataAccess(filePath, this);

    if(_sda->error().errorType() == DataAccessErrorType::None){
        QVariant list = _sda->execute("SELECT genre FROM genres;");
        foreach(QVariant var, list.toList()){
            QString genre = var.toMap()["genre"].toString().simplified();
            if(!_genres.contains(genre)){
                _genres.append(genre);
            }
        }
        // Add the data to the model
    }else{
        qDebug() << _sda->error().errorMessage();
    }
}

void Id3TagApplicationUI::showSavingDialog(){
    if(!_savingDialog){
        QmlDocument * qml = QmlDocument::create("asset:///SavingDialog.qml").parent(this);
        Container * container = qml->createRootObject<Container>();
        _savingDialog = Dialog::create().parent(this).content(container);
        connect(pps, SIGNAL(watchListEmpty()), _savingDialog, SLOT(close()), Qt::UniqueConnection);
    }
    _savingDialog->open();
}

void Id3TagApplicationUI::onDeleteFile(QString filepath){


    pps->addToWatchList(filepath);

    if(QFile::remove(filepath)){
        connect(pps, SIGNAL(watchListEmpty()), this, SLOT(refreshDataModels()), Qt::UniqueConnection);
        showSavingDialog();
    }else{
        pps->clearWatchList();

        SystemToast * toast = new SystemToast(this);
        toast->setBody(tr("%1 could not be deleted").arg(filepath));
        connect(toast, SIGNAL(finished(bb::system::SystemUiResult::Type)), toast, SLOT(deleteLater()));
        toast->show();
    }
}

