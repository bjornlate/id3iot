#ifndef Id3TagApplicationUI_HPP_
#define Id3TagApplicationUI_HPP_

#include <QObject>
#include <bb/system/InvokeManager>
#include <bb/cascades/AbstractPane>
#include <bb/data/SqlDataAccess>
#include "SearchableGroupDataModel.h"
#include "PaymentInterface.hpp"
#include "MediaChangesPpsObject.hpp"

namespace bb
{
    namespace cascades
    {
        class Application;
        class LocaleHandler;
        class DataModel;
        class Page;
        class ActionItem;
        class Dialog;
    }
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */
namespace id3iot {
class Id3TagApplicationUI : public QObject
{
    Q_OBJECT
signals:
    Q_SIGNAL void clearMMsyncRecords();
    Q_SIGNAL void updateMMsyncRecords(const QVariantList &);
    Q_SIGNAL void findAndUpdate(const QVariantMap &, const QVariantMap &);
    Q_SIGNAL void resetSearch();
public:
    Id3TagApplicationUI(bb::cascades::Application *app);
    virtual ~Id3TagApplicationUI();

    static const char * HAS_PRO_FEATURE;

private:
    Q_SLOT void onSystemLanguageChanged();
    Q_SLOT void playSong(QString fid, bool preview = true);
    Q_SLOT void onListTriggered(QVariantList indexPath);
    Q_SLOT void onInvoked(const bb::system::InvokeRequest &request);
    Q_SLOT void onCardDone();
    Q_SLOT void onDeleteFile(QString filepath);
    Q_SLOT void openMultipleEditPage(QVariant indexPaths);

    Q_INVOKABLE void onSave(QVariant indexPath, QVariant map);
    Q_SLOT void onSaveFinished(bool success, QVariantList indexPath, QVariantMap map);

    Q_INVOKABLE void onMultiSave(QVariant filePathToindexPath, QVariant tagUpdate);
    Q_SLOT void onMultiSaveFinished(bool success, QVariantMap fileToIndexPath, QVariantMap map);

    Q_SLOT void onProFeaturePurchasedMultiEdit(bool owned);
    Q_SLOT void showUpdatedTagsToast();

    Q_SLOT void showSavingDialog();

    Q_SLOT void onOwnsProFeatureChanged(bool proFeature);

    void buildGenreList();
    void initTabs();

    bool populateMap(QVariantMap & map);

    bb::cascades::Page* createId3EditPage(QVariantList indexPath, QVariantMap map);
    Q_SLOT void openId3EditSheet(QVariantList indexPath, QVariantMap map);
    Q_SLOT void openHelpSheet();

    QTranslator* translator;
    bb::cascades::LocaleHandler* localeHandler;
	bb::system::InvokeManager * _invokeManager;
	QVariantList _mmSyncRecords;

	QVariantList filterRecords(QVariantMap conditions, QVariantList source);
	bb::cascades::AbstractPane * createPage(QVariantList records, QStringList sortingKeys);

    void readTag(QString path);

    QString caseClean(const QString & source);
    void fetchMMSyncRecords(bb::data::SqlDataAccess * _sda);
    void fetchGenres(bb::data::SqlDataAccess * _sda);
    Q_SLOT void refreshDataModels();

    QString query;

    SearchableGroupDataModel * _currentDataModel;
    QStringList _genres;
    PaymentInterface * _paymentInterface;

    SearchableGroupDataModel * noArtistDataModel, * noAlbumDataModel, *allSongsDataModel, *genresDataModel, *noCoverArtDataModel;

    QThread workerThread;
    MediaChangesPpsObject * pps;
    QVariantMap matchMap;

    bb::data::SqlDataAccess * _sdaSD, * _sda;
    QSettings * _settings;
    bb::cascades::ActionItem * _proFeatureActionItem;
    bb::cascades::Dialog* _savingDialog;
};
}
#endif /* ApplicationUI_HPP_ */
