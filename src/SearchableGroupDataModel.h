/*
 * SearchableGroupDataModel.h
 *
 *  Created on: Jan 7, 2013
 *      Author: scottbirksted
 */

#ifndef SEARCHABLEGROUPDATAMODEL_H_
#define SEARCHABLEGROUPDATAMODEL_H_

#include <QObject>
#include <bb/cascades/DataModel>
#include <bb/cascades/GroupDataModel>


class SearchableGroupDataModel: public bb::cascades::DataModel {
	Q_OBJECT

	Q_PROPERTY(QVariantMap filters READ filters WRITE setFilters NOTIFY filtersChanged)
	Q_PROPERTY(int size READ size NOTIFY sizeChanged)
	Q_PROPERTY(bool busy READ busy NOTIFY busyChanged)
	Q_PROPERTY(QStringList sortingKeys READ sortingKeys WRITE setSortingKeys NOTIFY sortingKeysChanged)
signals:
	void isEmpty(bool);
	void filtersChanged(QVariantMap);
	void sizeChanged(int);
	void busyChanged(bool);

	Q_SIGNAL void sortingKeysChanged(QStringList);
	Q_SIGNAL void sortedAscendingChanged(bool);
	Q_SIGNAL void groupingChanged(bb::cascades::ItemGrouping::Type);

	Q_SIGNAL void modelChanged(bool);

public slots:
	void onDataLoaded(const QVariant & var);

	void setSearchQuery(QString query);
	void refresh();
	void removeAt(QVariantList indexPath);
	void removeList(QVariantList indexPaths);

	bool updateItem (const QVariantList &indexPath, const QVariantMap &item, bool refresh = true );
	bool updateItem (const QVariantMap &oldMap , const QVariantMap & newMap);

	QStringList sortingKeys();
	Q_SLOT void setSortingKeys(const QStringList & sortingKeys);

	Q_INVOKABLE QVariantList findExact (const QVariantMap &matchMap);
	void findAndUpdate(const QVariantMap & matchMap, const QVariantMap &item);

	void clear();
	void insertList(const QVariantList & items);
	void setFilter(QString filter, QVariant value);

	QVariantMap filters();
	void setFilters(QVariantMap filters = QVariantMap());
	int size();
	bool busy();
public:
	SearchableGroupDataModel(bb::cascades::GroupDataModel * sourceDataModel, QStringList searchableProperties, QObject *parent = 0);
	virtual ~SearchableGroupDataModel();

	bb::cascades::GroupDataModel * sourceDataModel();
	bb::cascades::GroupDataModel * activeDataModel();


    virtual int childCount(const QVariantList& indexPath);
    virtual bool hasChildren(const QVariantList& indexPath);
    virtual QVariant data(const QVariantList& indexPath);
    virtual QString itemType(const QVariantList& indexPath);

    void onRemoved(QVariantList indexPath);

    void insert(const QVariantMap & map);
private slots:
    void sizeUpdated();
private:
    bb::cascades::GroupDataModel * _sourceDataModel;
    bb::cascades::GroupDataModel * _searchedDataModel;
    QString searchQuery;
    QString filter;
    QVariantMap _filters;
    bool _busy;
    QStringList _searchableProperties;
};

#endif /* SEARCHABLEGROUPDATAMODEL_H_ */
