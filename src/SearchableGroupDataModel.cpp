/*
 * SearchableGroupDataModel.cpp
 *
 *  Created on: Jan 7, 2013
 *      Author: scottbirksted
 */

#include "SearchableGroupDataModel.h"

#include <QtCore>
#include <bb/cascades/ItemGrouping>
#include <bb/cascades/DataModelChangeType>

using namespace bb::cascades;

SearchableGroupDataModel::SearchableGroupDataModel(bb::cascades::GroupDataModel *sourceModel, QStringList searchableProperties, QObject *parent)
: bb::cascades::DataModel(parent),
  _sourceDataModel(sourceModel),
  _searchedDataModel(new GroupDataModel(this)),
  _busy(false),
  _searchableProperties(searchableProperties)
{
	_sourceDataModel->setParent(this);
	_searchedDataModel->setSortingKeys(_sourceDataModel->sortingKeys());
	_searchedDataModel->setGrouping(_sourceDataModel->grouping());

	connect(_sourceDataModel, SIGNAL(sortingKeysChanged(QStringList)), _searchedDataModel, SLOT(setSortingKeys(QStringList)));
	connect(_sourceDataModel, SIGNAL(sortedAscendingChanged(bool)), _searchedDataModel, SLOT(setSortedAscending(bool)));
	connect(_sourceDataModel, SIGNAL(groupingChanged(bb::cascades::ItemGrouping::Type)), _searchedDataModel, SLOT(setGrouping(bb::cascades::ItemGrouping::Type)));

	connect(_sourceDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)), Qt::UniqueConnection);
	connect(_searchedDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)), Qt::UniqueConnection);

	connect(_sourceDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)), Qt::UniqueConnection);
	connect(_searchedDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)), Qt::UniqueConnection);

	connect(_sourceDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)),Qt::UniqueConnection);
	connect(_searchedDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)),Qt::UniqueConnection);

	connect(_sourceDataModel, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), this, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), Qt::UniqueConnection);

	connect(_searchedDataModel, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), this, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), Qt::UniqueConnection);


	connect(this, SIGNAL(itemAdded(QVariantList)), this, SLOT(sizeUpdated()));
	connect(this, SIGNAL(itemRemoved(QVariantList)), this, SLOT(sizeUpdated()));
	connect(this, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type, QSharedPointer<bb::cascades::DataModel::IndexMapper>)), this, SLOT(sizeUpdated()));
	connect(_sourceDataModel, SIGNAL(sortingKeysChanged(QStringList)), this, SLOT(sizeUpdated()));
	connect(_sourceDataModel, SIGNAL(sortedAscendingChanged(bool)), this, SLOT(sizeUpdated()));
	connect(_sourceDataModel, SIGNAL(groupingChanged(bb::cascades::ItemGrouping::Type)), this, SLOT(sizeUpdated()));

}

SearchableGroupDataModel::~SearchableGroupDataModel() {
	// TODO Auto-generated destructor stub
}

GroupDataModel * SearchableGroupDataModel::sourceDataModel(){
	return _sourceDataModel;
}

void SearchableGroupDataModel::onDataLoaded(const QVariant & var){
	if(var.canConvert<QVariantList>()){
//		QVariantList list;
//		foreach(QVariant var2, list){
			_sourceDataModel->insertList(var.toList());
//		}
	}

}

int SearchableGroupDataModel::childCount(const QVariantList& indexPath){

	if(searchQuery.isEmpty() && _filters.isEmpty()){
		return _sourceDataModel->childCount(indexPath);
	}else{
		return _searchedDataModel->childCount(indexPath);
	}
}
bool SearchableGroupDataModel::hasChildren(const QVariantList& indexPath){
	if(searchQuery.isEmpty() && _filters.isEmpty()){
		return _sourceDataModel->hasChildren(indexPath);
	}else{
		return _searchedDataModel->hasChildren(indexPath);
	}
}
QVariant SearchableGroupDataModel::data(const QVariantList& indexPath){
	if(searchQuery.isEmpty() && _filters.isEmpty()){
		return _sourceDataModel->data(indexPath);
	}else{
		return _searchedDataModel->data(indexPath);
	}

}
QString SearchableGroupDataModel::itemType(const QVariantList& indexPath){
	if(searchQuery.isEmpty() && _filters.isEmpty()){
		return _sourceDataModel->itemType(indexPath);
	}else{
		return _searchedDataModel->itemType(indexPath);
	}
}

void SearchableGroupDataModel::refresh(){
	setSearchQuery(searchQuery);
}

void SearchableGroupDataModel::setSearchQuery(QString query){
	QTime timer;
	timer.start();
	if(this->searchQuery != query.trimmed()){

		disconnect(_searchedDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)));
		disconnect(_searchedDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)));
		disconnect(_searchedDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)));
		disconnect(_searchedDataModel, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
		            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), this, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
		            QSharedPointer<bb::cascades::DataModel::IndexMapper>)));



		this->searchQuery = query.trimmed();
		if(!searchQuery.isEmpty()){
			_searchedDataModel->clear();

			foreach(QVariantMap map, _sourceDataModel->toListOfMaps()){

				bool add = true;
				foreach(QVariant var, _filters.keys()){
					QString key = var.toString();
					QVariant value = _filters[key];
					if(map[key] != value){
						add = false;
						break;
					}
				}

				if(add){

					foreach(QString key, map.keys()){
						if(_searchableProperties.contains(key)){
							//key == "artist" || key == "title" || key == "label"){
							QVariant value = map[key];
							//exact match
							QString string = value.toString();
							if(searchQuery.startsWith("the ", Qt::CaseInsensitive)){
								qDebug() << "removing 'the ' ";
								searchQuery = searchQuery.right(searchQuery.length()-4);
								qDebug() << "now: " << searchQuery;
							}

							if(string.contains(searchQuery, Qt::CaseInsensitive)){
								_searchedDataModel->insert(map);
								break;
							}/*else{
							QStringList searchSplits = query.toLower().split(' ');
							searchSplits.removeAll("the");
							//word match
							int matches = 0;
							foreach(QString split, searchSplits){
								if(string.contains(split, Qt::CaseInsensitive)){
									++matches;
								}
							}
							if(matches == searchSplits.size()){
								m_searchedDataModel->insert(map);
								break;
							}
						}*/
						}
					}
				}
			}

			emit isEmpty(_searchedDataModel->size() == 0);
		}else{
			setFilters(_filters);
			emit isEmpty(_sourceDataModel->size() == 0);
			return;
		}

		connect(_searchedDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)), Qt::UniqueConnection);
		connect(_searchedDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)), Qt::UniqueConnection);
		connect(_searchedDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)),Qt::UniqueConnection);
		connect(_searchedDataModel, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
		            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), this, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
		            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), Qt::UniqueConnection);


		emit itemsChanged(bb::cascades::DataModelChangeType::Init);
		qDebug() << searchQuery << "Search time " << timer.elapsed();
	}
}

void SearchableGroupDataModel::onRemoved(QVariantList indexPath){
	emit itemRemoved(indexPath);
}

void SearchableGroupDataModel::removeAt(QVariantList indexPath){
	QVariantMap map = activeDataModel()->data(indexPath).toMap();
	if(activeDataModel() == _searchedDataModel){
		_searchedDataModel->remove(map);
	}
	_sourceDataModel->remove(map);
	emit modelChanged(true);
}

bool SearchableGroupDataModel::updateItem (const QVariantMap &oldMap, const QVariantMap &newMap){

	if(activeDataModel() == _searchedDataModel){
		//now we need to update the item in the search model.
		QVariantList indexPath = _searchedDataModel->findExact(oldMap);
		if(indexPath.isEmpty()){
			qDebug() << "Failed, could not find the matchMap in the search";
			return false;
		}

		bool removed = false;
		foreach(QString key, _filters.keys()){
			if(_filters[key] != newMap[key]){
				removed = true;
				disconnect(_sourceDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)));
				_searchedDataModel->removeAt(indexPath);
				connect(_sourceDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)), Qt::UniqueConnection);
				break;
			}
		}
		if(!removed){
			disconnect(_sourceDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)));
			_searchedDataModel->updateItem(indexPath, newMap);
		}
	}

	QVariantList srcIndexPath = _sourceDataModel->findExact(oldMap);
	if(srcIndexPath.isEmpty()){
		qDebug() << "Failed, could not find the matchMap in the source";
		return false;
	}
	_sourceDataModel->updateItem(srcIndexPath, newMap);
	//Reconnect the
	connect(_sourceDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)), Qt::UniqueConnection);

	emit modelChanged(true);
	return true;
}

QVariantList SearchableGroupDataModel::findExact (const QVariantMap &matchMap){
    GroupDataModel * model = activeDataModel();
    for ( QVariantList indexPath = model->first(); !indexPath.isEmpty(); indexPath = model->after(indexPath) ){
        QVariantMap map = model->data(indexPath).toMap();
        bool found = true;
        foreach(QString key, matchMap.keys()){
            if(map[key] != matchMap[key]){
                found = false;
                break;
            }
        }
        if(found){
            return indexPath;
        }
        // Do something with the item here.
    }
    return QVariantList();
}

void SearchableGroupDataModel::findAndUpdate(const QVariantMap & matchMap, const QVariantMap &item){
	qDebug() << "findAndUpdate...";

	GroupDataModel * model = activeDataModel();
	for ( QVariantList indexPath = model->first(); !indexPath.isEmpty(); indexPath = model->after(indexPath) )
	{
	    QVariantMap map = model->data(indexPath).toMap();

	    bool found = true;
	    foreach(QString key, matchMap.keys()){
	    	if(map[key] != matchMap[key]){
	    		found = false;
	    		break;
	    	}
	    }
	    if(found){
	    	model->updateItem(indexPath, item);
			qDebug() << "findAndUpdate done";

	    }
	}
}


bool SearchableGroupDataModel::updateItem (const QVariantList &indexPath, const QVariantMap &item, bool refresh ){
	bool retVal = false;
	if(searchQuery.isEmpty()){
		retVal = _sourceDataModel->updateItem(indexPath, item);
	}else{
		QVariant var = _searchedDataModel->data(indexPath);
		if(var.isValid() && var.canConvert<QVariantMap>()){
			QVariantList srcIndexPath = _sourceDataModel->findExact(var.toMap());
			if(srcIndexPath.length() > 0){
				retVal = _sourceDataModel->updateItem(srcIndexPath, item);
			}
			if(refresh){
				this->refresh();
				//	emit itemsChanged(bb::cascades::DataModelChangeType::Init);
			}
		}
	}
	emit modelChanged(true);
	return retVal;
}


void SearchableGroupDataModel::removeList(QVariantList indexPaths){
	qDebug() << "Size: " << indexPaths.size();
	for(int i=indexPaths.size()-1; i>= 0; --i){
		if(indexPaths.at(i).canConvert<QVariantList>()){
			removeAt(indexPaths.at(i).toList());
		}
	}
}

void SearchableGroupDataModel::insert(const QVariantMap & map){
	_sourceDataModel->insert(map);
	refresh();
	emit modelChanged(true);
}

void SearchableGroupDataModel::clear(){
    if(_sourceDataModel){
        _sourceDataModel->clear();
    }
    if(_searchedDataModel){
        _searchedDataModel->clear();
    }
	filter.clear();
	refresh();
}


void SearchableGroupDataModel::insertList(const QVariantList & items){
	//disconnect(m_sourceDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)));//, Qt::UniqueConnection);

	_sourceDataModel->insertList(items);

	//connect(m_sourceDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)), Qt::UniqueConnection);

}

void SearchableGroupDataModel::setFilter(QString filter, QVariant value){

	if(!filter.isEmpty()){
		foreach(QVariantMap map, _sourceDataModel->toListOfMaps()){
			if(map[filter] == value){
				_searchedDataModel->insert(map);
			}
		}
	}
}


QVariantMap SearchableGroupDataModel::filters(){
	return _filters;
}
void SearchableGroupDataModel::setFilters(QVariantMap filters){
	_filters = filters;
	_searchedDataModel->clear();

	_busy = true;
	emit busyChanged(_busy);

	disconnect(_sourceDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)));//, Qt::UniqueConnection);
	disconnect(_searchedDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)));//, Qt::UniqueConnection);

	foreach(QVariantMap map, _sourceDataModel->toListOfMaps()){
		bool add = true;
		foreach(QVariant var, _filters.keys()){
			QString key = var.toString();
			QVariant value = filters[key];
			if(map[key] != value){
				add = false;
				break;
			}
		}
		if(add){
			_searchedDataModel->insert(map);
		}
	}
	emit itemsChanged(bb::cascades::DataModelChangeType::Init);
	connect(_sourceDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)), Qt::UniqueConnection);
	connect(_searchedDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)), Qt::UniqueConnection);

	_busy = false;

	emit busyChanged(_busy);
}

GroupDataModel * SearchableGroupDataModel::activeDataModel(){
	if(searchQuery.isEmpty() && _filters.isEmpty()){
		return _sourceDataModel;
	}else{
		return _searchedDataModel;
	}

}

int SearchableGroupDataModel::size(){
	return activeDataModel()->size();
}

void SearchableGroupDataModel::sizeUpdated(){
	emit sizeChanged(size());
}

bool SearchableGroupDataModel::busy(){
	return _busy;
}


QStringList SearchableGroupDataModel::sortingKeys(){
    return activeDataModel()->sortingKeys();
}

void SearchableGroupDataModel::setSortingKeys(const QStringList & keys){
    _sourceDataModel->setSortingKeys(keys);
    _searchedDataModel->setSortingKeys(keys);
}
