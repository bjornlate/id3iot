#include <bb/cascades/Application>

#include <QLocale>
#include <QTranslator>
#include "Id3TagApplicationUI.hpp"

#include <Qt/qdeclarativedebug.h>
#include "PictureEditorLauncher.hpp"
#include "id3iot/CoverArtSearch.hpp"

#include "HTTPImage.hpp"

using namespace bb::cascades;
using namespace id3iot;

void message_handler(QtMsgType type, const char *msg) {
	switch (type) {
		case QtDebugMsg:
			fprintf(stderr, "DBG: %s\n", msg);
			break;
		case QtWarningMsg:
			fprintf(stderr, "WRN: %s\n", msg);
			break;
		case QtCriticalMsg:
			fprintf(stderr, "CRT: %s\n", msg);
			break;
		case QtFatalMsg:
			fprintf(stderr, "FAT: %s\n", msg);
			break;
	}
}


Q_DECL_EXPORT int main(int argc, char **argv)
{

	qInstallMsgHandler(message_handler);

    Application app(argc, argv);

    qmlRegisterType<PictureEditorLauncher>("com.id3iot", 1, 0, "PictureEditorLauncher");
    qmlRegisterType<CoverArtSearch>("com.id3iot", 1, 0, "CoverArtSearch");
    qmlRegisterType<HTTPImage>("com.id3iot", 1, 0, "HTTPImage");

    // Create the Application UI object, this is where the main.qml file
    // is loaded and the application scene is set.
    new Id3TagApplicationUI(&app);

    // Enter the application main event loop.
    return Application::exec();
}
