//Using PictureHeaderLauncher.h header file

#ifndef PICTUREEDITORLAUNCHER_H_
#define PICTUREEDITORLAUNCHER_H_

#include <QObject>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeTargetReply.hpp>
#include <bb/system/CardDoneMessage.hpp>

class PictureEditorLauncher : public QObject {
    Q_OBJECT
    Q_PROPERTY (QString pictureUrl READ pictureUrl NOTIFY pictureUrlChanged);
    Q_SIGNAL void pictureUrlChanged(const QString & url);
public:
    PictureEditorLauncher(QObject* pParent = NULL);
    virtual ~PictureEditorLauncher();

    Q_INVOKABLE void doLaunch (const QString &app, const QString &msg, const QString &sizeString, bool upScale = true, bool delTmpFile = true);

public slots:
    void childCardDone(const bb::system::CardDoneMessage& cardDoneMessage);

    void invokeRequestFinished();
    Q_INVOKABLE QString pictureUrl();

private:
    bool mDeleteTmpFile;
    bb::system::InvokeManager* m_invokeManager;
    bb::system::InvokeTargetReply* m_targetReply;
    QString _pictureUrl;
};

#endif /* PICTUREEDITORLAUNCHER_H_ */
