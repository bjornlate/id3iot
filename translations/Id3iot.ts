<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>CoverArt</name>
    <message>
        <location filename="../assets/CoverArt.qml" line="81"/>
        <source>Cover Art Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="82"/>
        <source>If the Cover Art picture is already on your BlackBerry choose Local. Otherwise choose Search to look for the Cover Art on the internet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="83"/>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="84"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="108"/>
        <source>Select Cover Art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="145"/>
        <source>Delete the cover art for this song?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="146"/>
        <source>Delete Cover Art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="147"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="148"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <location filename="../assets/HelpPage.qml" line="5"/>
        <source>Help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/HelpPage.qml" line="7"/>
        <source>Close</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/HelpPage.qml" line="20"/>
        <source>This app is designed to help you organize your music library through correcting your ID3 tags.

The tabs in the app show you files that are missing critical information that the Music app uses to group and display your collection.

No Album - songs missing an Album name
No Artist - songs missing an Artist name
All Songs - everything in your collection, sorted by Album Name

If you&apos;re looking for a song in particular, just type it in the search box and the list will return any songs that contain that phrase in the song title, artist or album name.

Click on a song and the ID3 Tag Edit page will open allowing you to correct the files information.

Tap &apos;Save&apos; to update the file or &apos;Cancel&apos; to discard the changes.
The music player will recognize saved files momentarily.

If you want to edit tags directly from the Music player or File manager, long press on the song/file and choose &apos;Open in&apos; ID3IOT.
The Tag Edit page will appear and you can update your tag from there.

Pro Upgrade Features:
Edit Multiple Tags - You can use the &apos;Select Items&apos; menu option to choose more than one file to edit at a time.
To change the property for all selected songs, toggle its associated checkbox.
Tap &apos;Save&apos; to update all the selected files, listed at the bottom of the Edit Tags screen.

Edit Cover Art - Open the tag editor. If you see a pencil icon in the corner of the Cover Art image, you can edit it.
Tap the pencil and you will be presented with a dialog that lets you select a local image as the cover art.
Alternatively you can open the web browser and search manually for a cover art image.
When you have saved an image to your BlackBerry, tap the pencil again and now choose the Local file option.
Cover art images must be square and under 700x700 px. If the cover art image you selected does not meet those conditions the photo editor will open so you can adjust the image and save a copy that meets the critera. This feature works for MP3 files.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Id3TagMultiPage</name>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="16"/>
        <source>Edit Cover Art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="39"/>
        <source>Edit ID3 Tags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="41"/>
        <source>Save</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="74"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="112"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="127"/>
        <source>Artist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="134"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="149"/>
        <source>Album</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="163"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="178"/>
        <source>Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="192"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="198"/>
        <source>Disc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="207"/>
        <source>Compilation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="231"/>
        <source>Genre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="240"/>
        <source>Custom Genre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="252"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="269"/>
        <source>Comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="278"/>
        <source>Files</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Id3TagPage</name>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="16"/>
        <source>Edit Cover Art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="38"/>
        <source>Edit ID3 Tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="40"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="73"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="79"/>
        <source>Play Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="88"/>
        <source>Open in Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="97"/>
        <location filename="../assets/Id3TagPage.qml" line="105"/>
        <source>Song as Album Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="102"/>
        <source>Normal Album Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="114"/>
        <source>Are you sure you want to delete %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="115"/>
        <source>Delete File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="151"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="174"/>
        <source>Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="184"/>
        <source>Sample Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="200"/>
        <location filename="../assets/Id3TagPage.qml" line="216"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="221"/>
        <location filename="../assets/Id3TagPage.qml" line="227"/>
        <source>Artist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="244"/>
        <location filename="../assets/Id3TagPage.qml" line="250"/>
        <source>Album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="257"/>
        <location filename="../assets/Id3TagPage.qml" line="263"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="276"/>
        <location filename="../assets/Id3TagPage.qml" line="282"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="294"/>
        <location filename="../assets/Id3TagPage.qml" line="300"/>
        <source>Disc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="310"/>
        <source>Compilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="324"/>
        <source>Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="335"/>
        <source>Custom Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="346"/>
        <location filename="../assets/Id3TagPage.qml" line="353"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageSearchPage</name>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="84"/>
        <source>Cover Art Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="85"/>
        <source>Are you sure this is the cover art you want to use?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="86"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="87"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SavingDialog</name>
    <message>
        <location filename="../assets/SavingDialog.qml" line="32"/>
        <source>Updating tags...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>id3iot::Id3TagApplicationUI</name>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="184"/>
        <source>No Album</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="199"/>
        <source>No Artist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="214"/>
        <source>No Cover Art</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="229"/>
        <source>All Songs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="267"/>
        <source>Refresh</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="272"/>
        <source>Pro Upgrade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="333"/>
        <source>Card: I am done. yay!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="335"/>
        <source>Success!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="490"/>
        <source>Tag(s) are updated, but are no longer in this view.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="1007"/>
        <source>Tag updates complete.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="1083"/>
        <source>%1 could not be deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="909"/>
        <source>Error saving tag</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="978"/>
        <source>Error saving tags</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>id3iot::PaymentInterface</name>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="67"/>
        <source>Upgrade to IDIOT Pro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="68"/>
        <source>To perform this action, you need to purchase the upgrade to ID3IOT Pro.

With ID3IOT Pro you can edit multiple tags at a time and also edit Cover Art!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="97"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="98"/>
        <source>The audio tag changes were discarded because Pro feature was not purchased</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="36"/>
        <source>Hide Sort Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="36"/>
        <source>Show Sort Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="87"/>
        <source>By Album</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="95"/>
        <source>By Genre</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="103"/>
        <source>By Artist</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="124"/>
        <source>There are no songs to display in this list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="145"/>
        <source>Unknown</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="162"/>
        <source>Play Song</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="173"/>
        <source>Open in Music</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="189"/>
        <source>Edit Tags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="209"/>
        <source>Search for Song, Artist, Album or Genre name</source>
        <translation></translation>
    </message>
</context>
</TS>
