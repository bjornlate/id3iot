<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>CoverArt</name>
    <message>
        <location filename="../assets/CoverArt.qml" line="81"/>
        <source>Cover Art Location</source>
        <translation>封面位置</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="82"/>
        <source>If the Cover Art picture is already on your BlackBerry choose Local. Otherwise choose Search to look for the Cover Art on the internet.</source>
        <translation>如果封面图片已经在您的BlackBerry选择本地。否则，选择搜索，寻找封面艺术在互联网上。</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="83"/>
        <source>Local</source>
        <translation>当地</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="84"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="108"/>
        <source>Select Cover Art</source>
        <translation>选择封面</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="145"/>
        <source>Delete the cover art for this song?</source>
        <translation>删除封面？</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="146"/>
        <source>Delete Cover Art</source>
        <translation>删除封面？</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="147"/>
        <source>OK</source>
        <translation>行</translation>
    </message>
    <message>
        <location filename="../assets/CoverArt.qml" line="148"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <location filename="../assets/HelpPage.qml" line="5"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../assets/HelpPage.qml" line="7"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../assets/HelpPage.qml" line="20"/>
        <source>This app is designed to help you organize your music library through correcting your ID3 tags.

The tabs in the app show you files that are missing critical information that the Music app uses to group and display your collection.

No Album - songs missing an Album name
No Artist - songs missing an Artist name
All Songs - everything in your collection, sorted by Album Name

If you&apos;re looking for a song in particular, just type it in the search box and the list will return any songs that contain that phrase in the song title, artist or album name.

Click on a song and the ID3 Tag Edit page will open allowing you to correct the files information.

Tap &apos;Save&apos; to update the file or &apos;Cancel&apos; to discard the changes.
The music player will recognize saved files momentarily.

If you want to edit tags directly from the Music player or File manager, long press on the song/file and choose &apos;Open in&apos; ID3IOT.
The Tag Edit page will appear and you can update your tag from there.

Pro Upgrade Features:
Edit Multiple Tags - You can use the &apos;Select Items&apos; menu option to choose more than one file to edit at a time.
To change the property for all selected songs, toggle its associated checkbox.
Tap &apos;Save&apos; to update all the selected files, listed at the bottom of the Edit Tags screen.

Edit Cover Art - Open the tag editor. If you see a pencil icon in the corner of the Cover Art image, you can edit it.
Tap the pencil and you will be presented with a dialog that lets you select a local image as the cover art.
Alternatively you can open the web browser and search manually for a cover art image.
When you have saved an image to your BlackBerry, tap the pencil again and now choose the Local file option.
Cover art images must be square and under 700x700 px. If the cover art image you selected does not meet those conditions the photo editor will open so you can adjust the image and save a copy that meets the critera. This feature works for MP3 files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This app is designed to help you organize your music library through correcting your ID3 tags.

The tabs in the app show you files that are missing critical information that the Music app uses to group and display your collection.

No Album - songs missing an Album name
No Artist - songs missing an Artist name
All Songs - everything in your collection, sorted by Album Name

If you&apos;re looking for a song in particular, just type it in the search box and the list will return any songs that
contain that phrase in the song title, artist or album name.

Click on a song and the ID3 Tag Edit page will open allowing you to correct the files information.

Tap &apos;Save&apos; to update the file or &apos;Cancel&apos; to discard the changes. 
The music player will recognize saved files momentarily.

If you want to edit tags directly from the Music player or File manager, long press on the song/file and choose &apos;Open in&apos; ID3IOT. 
The Tag Edit page will appear and you can update your tag from there.

Pro Upgrade Features:
Edit Multiple Tags - You can use the &apos;Select Items&apos; menu option to choose more than one file to edit at a time.
To change the property for all selected songs, toggle its associated checkbox.
Tap &apos;Save&apos; to update all the selected files, listed at the bottom of the Edit Tags screen.

Edit Cover Art - Open the tag editor. If you see a pencil icon in the corner of the Cover Art image, you can edit it. 
Tap the pencil and you will be presented with a dialog that lets you select a local image as the cover art.
Alternatively you can open the web browser and search manually for a cover art image. 
When you have saved an image to your BlackBerry, tap the pencil again and now choose the Local file option.
Cover art images must be square and under 700x700 px. If the cover art image you selected does not meet those conditions the photo
editor will open so you can adjust the image and save a copy that meets the critera. 
This feature works for MP3 files.  
</source>
        <translation type="obsolete">帮助文本：
本程序可以通过更正音乐的ID3IOT３标签来帮助您管理您的音乐库。
程序可以显示您音乐文件中丢失的重要信息，这些信息通常用来在音乐程序中分组，显示专辑等。
No Album-文件无专辑名
No Artist-文件无艺术家
All Songs-您收藏中的全部音乐，按专辑名分类排序

如果您在寻找某一特定音乐，只须在搜索框中输入指定内容，所有在音乐名、艺术家和专辑名中包含指定内容的信息都会出现。

在音乐上单击，ID3IOT3标签编辑页面就会打开，您可以对相关信息进行更正。

单击“保存”保存更改信息，或“取消”放弃更改。音乐播放器会对保存的文件信息即刻做出识别。

如果您想在音乐播放器或文件管理器中直接编辑音乐标签，您仅须在音乐或文件上长按，选择“在ID3IOT中打开”。

标签编辑界面会出现，您可以直播在这里更新您的音乐标签。

专业版特色功能：
多标签编辑-您可以使用“选择项目”菜单实现一次多标签编辑。
选中复选框，更改所有音乐文件属性。

单击“保存”更新选中文件信息，相关信息会在编辑标签屏幕下方显示。

编辑专辑封面-在标签编辑器中打开文件，如果在专辑封面图像一角出现铅笔图标，表示您可以对其进行编辑。
单击铅笔图标，会弹出一个对话框，让您选择本地图片作为专辑封面。您也可以通过浏览器手动搜索专辑封面图像。如果您在黑莓设备中保存有图像，您可以再次点击铅笔图标选择本地文件选项。
专辑封面图像要求正方形，像素在700*700以下。如果您选择的图像不符合上述要求，图片编辑器会打开，您可以在其中对图片进行编辑以满足要求。

上述功能只针对MP3文件。
</translation>
    </message>
</context>
<context>
    <name>Id3TagMultiPage</name>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="16"/>
        <source>Edit Cover Art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="39"/>
        <source>Edit ID3 Tags</source>
        <translation>编辑ID3标签</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="41"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="74"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="112"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="127"/>
        <source>Artist</source>
        <translation>艺术家</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="134"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="149"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="163"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="178"/>
        <source>Year</source>
        <translation>年份</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="192"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="198"/>
        <source>Disc</source>
        <translation>圆盘</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="207"/>
        <source>Compilation</source>
        <translation>汇编</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="231"/>
        <source>Genre</source>
        <translation>流派</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="240"/>
        <source>Custom Genre</source>
        <translation>自定义流派</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="252"/>
        <location filename="../assets/Id3TagMultiPage.qml" line="269"/>
        <source>Comment</source>
        <translation>评语</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagMultiPage.qml" line="278"/>
        <source>Files</source>
        <translation>档</translation>
    </message>
</context>
<context>
    <name>Id3TagPage</name>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="16"/>
        <source>Edit Cover Art</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="38"/>
        <source>Edit ID3 Tag</source>
        <translation>编辑ID3标签</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="40"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="73"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="79"/>
        <source>Play Song</source>
        <translation>播放歌曲</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="88"/>
        <source>Open in Music</source>
        <translation>打开音乐</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="97"/>
        <location filename="../assets/Id3TagPage.qml" line="105"/>
        <source>Song as Album Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="102"/>
        <source>Normal Album Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="114"/>
        <source>Are you sure you want to delete %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="115"/>
        <source>Delete File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="151"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="174"/>
        <source>Bitrate</source>
        <translation>比特率</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="184"/>
        <source>Sample Rate</source>
        <translation>采样率</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="200"/>
        <location filename="../assets/Id3TagPage.qml" line="216"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="221"/>
        <location filename="../assets/Id3TagPage.qml" line="227"/>
        <source>Artist</source>
        <translation>艺术家</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="244"/>
        <location filename="../assets/Id3TagPage.qml" line="250"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="257"/>
        <location filename="../assets/Id3TagPage.qml" line="263"/>
        <source>Track</source>
        <translation>轨道</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="276"/>
        <location filename="../assets/Id3TagPage.qml" line="282"/>
        <source>Year</source>
        <translation>年份</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="294"/>
        <location filename="../assets/Id3TagPage.qml" line="300"/>
        <source>Disc</source>
        <translation>圆盘</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="310"/>
        <source>Compilation</source>
        <translation>汇编</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="324"/>
        <source>Genre</source>
        <translation>流派</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="335"/>
        <source>Custom Genre</source>
        <translation>自定义流派</translation>
    </message>
    <message>
        <location filename="../assets/Id3TagPage.qml" line="346"/>
        <location filename="../assets/Id3TagPage.qml" line="353"/>
        <source>Comment</source>
        <translation>评语</translation>
    </message>
</context>
<context>
    <name>ImageSearchPage</name>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="84"/>
        <source>Cover Art Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="85"/>
        <source>Are you sure this is the cover art you want to use?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="86"/>
        <source>OK</source>
        <translation type="unfinished">行</translation>
    </message>
    <message>
        <location filename="../assets/ImageSearchPage.qml" line="87"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>PaymentInterface</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">行</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">行</translation>
    </message>
</context>
<context>
    <name>SavingDialog</name>
    <message>
        <location filename="../assets/SavingDialog.qml" line="32"/>
        <source>Updating tags...</source>
        <translation>更新标签...</translation>
    </message>
</context>
<context>
    <name>id3iot::Id3TagApplicationUI</name>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="184"/>
        <source>No Album</source>
        <translation>无专辑</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="199"/>
        <source>No Artist</source>
        <translation>无艺术家</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="214"/>
        <source>No Cover Art</source>
        <translation>无封面</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="229"/>
        <source>All Songs</source>
        <translation>所有歌曲</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="267"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="272"/>
        <source>Pro Upgrade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="333"/>
        <source>Card: I am done. yay!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="335"/>
        <source>Success!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="490"/>
        <source>Tag(s) are updated, but are no longer in this view.</source>
        <translation>标签被更新，但不再在该视图中</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="909"/>
        <source>Error saving tag</source>
        <translation>错误节能标签</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="978"/>
        <source>Error saving tags</source>
        <translation>保存时出错标签</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="1007"/>
        <source>Tag updates complete.</source>
        <translation>标签更新完成</translation>
    </message>
    <message>
        <location filename="../src/id3iot/Id3TagApplicationUI.cpp" line="1083"/>
        <source>%1 could not be deleted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>id3iot::PaymentInterface</name>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="67"/>
        <source>Upgrade to IDIOT Pro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="68"/>
        <source>To perform this action, you need to purchase the upgrade to ID3IOT Pro.

With ID3IOT Pro you can edit multiple tags at a time and also edit Cover Art!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="97"/>
        <source>OK</source>
        <translation type="unfinished">行</translation>
    </message>
    <message>
        <location filename="../src/id3iot/PaymentInterface.cpp" line="98"/>
        <source>The audio tag changes were discarded because Pro feature was not purchased</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="36"/>
        <source>Hide Sort Options</source>
        <translation>隐藏排序选项</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="36"/>
        <source>Show Sort Options</source>
        <translation>显示排序选项</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="87"/>
        <source>By Album</source>
        <translation type="unfinished">按专辑</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="95"/>
        <source>By Genre</source>
        <translation type="unfinished">按流派</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="103"/>
        <source>By Artist</source>
        <translation>按艺术家</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="124"/>
        <source>There are no songs to display in this list</source>
        <translation>没有歌曲在此列表中显示</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="145"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="162"/>
        <source>Play Song</source>
        <translation>播放歌曲</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="173"/>
        <source>Open in Music</source>
        <translation>打开音乐</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="189"/>
        <source>Edit Tags</source>
        <translation>编辑标签</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="209"/>
        <source>Search for Song, Artist, Album or Genre name</source>
        <translation>搜索歌曲，艺术家，专辑，流派</translation>
    </message>
</context>
</TS>
