import bb.cascades 1.0
import bb.data 1.0
import bb.system 1.0
import com.id3iot 1.0

Page {
    property string initialSearchQuery;
    property variant chosenImage;
    signal selectImage(string filePath)
    
    titleBar: TitleBar {
        title: "Cover Art Search"
    }
    
    onInitialSearchQueryChanged: {
        searchField.setText(initialSearchQuery)
        search();
    }
    
    function search(){
        noResultsMessage.visible = false;
        searchSpinner.start();
        dataSource.start = 1;
        dataModel.clear();
        dataSource.source = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + searchField.text
    }
    
    attachedObjects: [
        GroupDataModel {
            id: dataModel
            grouping: ItemGrouping.ByFullValue
            sortingKeys: [ "type", "order" ]
        },
        DataSource {
            id: dataSource
            //source: "asset:///gimages.json"
            remote: true
            type: DataSourceType.Json
            //source: "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=fuzzy%20monkey"
            property string rootSource: "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q="
            property int start: 1
            
            onDataLoaded: {
                console.log("onDataLoaded" + dataSource.source);
                searchSpinner.stop()
                //var results = data.responseData.results;
                //for(var i=0; i< results.length; ++i){
                if (data.responseData == undefined || data.responseData.results.length == 0) {
                    noResultsMessage = true;
                } else {
                    for (var i = 0; i < data.responseData.results.length; ++ i) {
                        var item = data.responseData.results[i];
                        item.type = "image";
                        item.order = start ++;
                        dataModel.insert(item);
                    }
                    console.log("start = " + start);
                    //Load more results
                    var item = {
                        "type": "more",
                        "order": 0,
                        "next": (rootSource + searchField.text + "&start=" + start)
                    };
                    dataModel.insert(item);
                
                }
            }
            onSourceChanged: {
                console.log("Source fucking changed " + source);
                load();
            }
        },
        HTTPImage {
            id: httpDownloader
            onImageDownloadComplete: {
                listContainer.enabled = true
                searchSpinner.stop()
                var fullPath = httpDownloader.saveImage("imagesearch");
                selectImage(fullPath);
            }
        },
        SystemDialog {
            id: coverArtConfirmDialog
            title: qsTr("Cover Art Selection")
            body: qsTr("Are you sure this is the cover art you want to use?")
            confirmButton.label: qsTr("OK")
            cancelButton.label: qsTr("Cancel")
            onFinished: {
                if(result == SystemUiResult.ConfirmButtonSelection){
                    //save it.
                    listView.visible = false
                    searchSpinner.start()
                    listContainer.enabled = false
                    httpDownloader.url = chosenImage.url;
                }
            }
        }
    ]
    onCreationCompleted: {
        //dataSource.load();
    }
    
    Container {
        layout: DockLayout {
        }
        
        ActivityIndicator {
            preferredWidth: 350
            id: searchSpinner
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
        }
        
        Container {
            id: noResultsMessage
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            preferredWidth: 500
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                
                text: "There are no Cover Art image results for this search"
                textStyle.fontSize: FontSize.XLarge
                textStyle.fontWeight: FontWeight.W300
                textStyle.textAlign: TextAlign.Center
                multiline: true
            }
        }
        Container {
            id: listContainer
            ListView {
                id: listView
                objectName: "listView"
                dataModel: dataModel
                layout: FlowListLayout {
                    headerMode: ListHeaderMode.Sticky
                    //cellAspectRatio: 0.65
                    //columnCount: 3
                }
                onTriggered: {
                    if (dataModel.data(indexPath).type == "more") {
                        dataSource.source = dataModel.data(indexPath).next;
                        dataModel.removeAt(indexPath)
                    } else if (dataModel.data(indexPath).type == "image") {
                        chosenImage = dataModel.data(indexPath);
                        coverArtConfirmDialog.show();
                    }
                }
                listItemComponents: [
                    ListItemComponent {
                        type: "more"
                        Container {
                            layoutProperties: FlowListLayoutProperties {
                                fillRatio: 1
                            }
                            Button {
                                horizontalAlignment: HorizontalAlignment.Fill
                                text: "More"
                                touchPropagationMode: TouchPropagationMode.None
                            }
                        }
                    },
                    ListItemComponent {
                        type: "image"
                        ImageSearchListItem {
                            src: ListItemData.tbUrl //ListItemData.url
                            layoutProperties: FlowListLayoutProperties {
                                aspectRatio: 0.65
                                fillRatio: 1 / 2
                            }
                        }
                    }, //we get a header from the rss feed, but we dont want to display it
                    ListItemComponent {
                        type: "header"
                        Container {
                        
                        }
                    }
                ]
                //accessibility.name: "Listing"
                // Item type-mapping
                function itemType(data, indexPath) {
                    if (indexPath.length == 1) {
                        return 'header';
                    } else {
                        return data.type;
                        /*if (data.url != undefined && data.url != "") {
                         * // The data contain an image return an item that can display an image.
                         * return 'imageItem'
                         * } else {
                         * // No image in the data display a text item.
                         * return 'item'
                         }*/
                    }
                }
            }
            Container {
                Divider {
                }
                bottomPadding: 12
                leftPadding: 12
                rightPadding: 12
                verticalAlignment: VerticalAlignment.Bottom
                TextField {
                    id: searchField
                    objectName: "searchField"
                    hintText: "Search for Song, Artist or Album"
                    input.submitKey: SubmitKey.Search
                    input.onSubmitted: {
                        search();
                    }
                }
            }
        }
    }
}
