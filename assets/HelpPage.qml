import bb.cascades 1.0

Page {
    titleBar: TitleBar {
        title: qsTr("Help")
        acceptAction: ActionItem{
            title: qsTr("Close")
        }
    }
    content: ScrollView{
        content:
	    Container {
	        topPadding: 12
	        bottomPadding: 12
	        rightPadding: 12
	        leftPadding: 12
	        
	    	Label {
	    	    multiline: true
	    	    text: qsTr("This app is designed to help you organize your music library through correcting your ID3 tags.\n\nThe tabs in the app show you files that are missing critical information that the Music app uses to group and display your collection.\n\nNo Album - songs missing an Album name\nNo Artist - songs missing an Artist name\nAll Songs - everything in your collection, sorted by Album Name\n\nIf you're looking for a song in particular, just type it in the search box and the list will return any songs that contain that phrase in the song title, artist or album name.\n\nClick on a song and the ID3 Tag Edit page will open allowing you to correct the files information.\n\nTap 'Save' to update the file or 'Cancel' to discard the changes.\nThe music player will recognize saved files momentarily.\n\nIf you want to edit tags directly from the Music player or File manager, long press on the song/file and choose 'Open in' ID3IOT.\nThe Tag Edit page will appear and you can update your tag from there.\n\nPro Upgrade Features:\nEdit Multiple Tags - You can use the 'Select Items' menu option to choose more than one file to edit at a time.\nTo change the property for all selected songs, toggle its associated checkbox.\nTap 'Save' to update all the selected files, listed at the bottom of the Edit Tags screen.\n\nEdit Cover Art - Open the tag editor. If you see a pencil icon in the corner of the Cover Art image, you can edit it.\nTap the pencil and you will be presented with a dialog that lets you select a local image as the cover art.\nAlternatively you can open the web browser and search manually for a cover art image.\nWhen you have saved an image to your BlackBerry, tap the pencil again and now choose the Local file option.\nCover art images must be square and under 700x700 px. If the cover art image you selected does not meet those conditions the photo editor will open so you can adjust the image and save a copy that meets the critera. This feature works for MP3 files.");
	    	}
	    }
	}
}
