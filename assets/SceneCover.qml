import bb.cascades 1.0

Container {
    layout: DockLayout{}
    background: Color.White
    ImageView {
        imageSource: "asset:///splashscreens/splash_720x720.png"
        scalingMethod: ScalingMethod.AspectFit
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
    }
}
