import bb.cascades 1.2
import com.id3iot 1.0

Container {
    property alias src: httpImage.url

    
    Container {

        id: root

        layout: DockLayout {
        }
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill
        
    
     
        ImageView {
            id: imageView
            objectName: "imageView"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            imageSource: "asset:///images/loading_image.png"
            scalingMethod: ScalingMethod.AspectFill
        }
    }
    Container {
        leftPadding: 12
        rightPadding: 12        
        Label {
            id: title
            text: ListItemData.title
            textFormat: TextFormat.Html
            textStyle.fontSize: FontSize.XSmall
            multiline: true
            autoSize.maxLineCount: 2
        }
        Label {
            topMargin: 0
            text: ListItemData.width+"x"+ListItemData.height
            textStyle.fontSize: FontSize.XSmall
        }
    }
    attachedObjects: [
        LayoutUpdateHandler {
            id: layoutUpdateHandler
            onLayoutFrameChanged: {
               root.minHeight = layoutFrame.width 
               root.minWidth = layoutFrame.width
               root.maxHeight = layoutFrame.width 
               root.maxWidth = layoutFrame.width
               root.preferredHeight = layoutFrame.width 
               root.preferredWidth = layoutFrame.width
            }
        },
        HTTPImage {
            id: httpImage
            objectName: "httpImage"
            onImageDownloadComplete: {
                imageView.image = image;
            }
            onUrlChanged: {
                imageView.imageSource = "asset:///images/loading_image.png";
            }
        }
    ]
}
