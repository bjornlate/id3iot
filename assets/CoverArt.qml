import bb.cascades 1.0
import bb.system 1.0
import bb.cascades.pickers 1.0
import com.id3iot 1.0

Container {
    layout: DockLayout {}

	property bool editable: false
    
    property string newCoverArtURL;
    property bool deleted: false;
    property string album;
    property string artist;
    
    minWidth: 300
    minHeight: 300
    
    id: coverArtContainer
    
    onNewCoverArtURLChanged: {
    	if(newCoverArtURL.length > 0){
    	    deleted = false;
    	}    
    }
    
    function editCoverArt(){
        if(!PaymentInterface.ownsProFeature){
            PaymentInterface.purchaseProFeature();
        }else{
            //coverArtPicker.open();
            pictureLocationDialog.show()
        }
    }
    
    ImageView {
        id: coverArt
        objectName: "coverArt"
        //accessibility.name: "cover art"
        imageSource: "asset:///images/default_cover_art.png"
        preferredHeight: 320
        preferredWidth: 320
        scalingMethod: ScalingMethod.AspectFill
        implicitLayoutAnimationsEnabled: false
    }
    ImageView {
        imageSource: "asset:///images/black_corner.png"
        verticalAlignment: VerticalAlignment.Bottom
        horizontalAlignment: HorizontalAlignment.Right
        scalingMethod: ScalingMethod.AspectFit
        preferredWidth: 130
        opacity: 0.6
        visible: editButton.visible
    }
    
    ImageView {
        imageSource: "asset:///images/black_corner.png"
        verticalAlignment: VerticalAlignment.Bottom
        horizontalAlignment: HorizontalAlignment.Left
        scalingMethod: ScalingMethod.AspectFit
        preferredWidth: 130
        opacity: 0.6
        rotationZ: 90
        visible: deleteImageButton.visible
    }
    ImageButton {
        visible: editable
        id: editButton
        defaultImageSource: "asset:///images/ic_edit.png"
        verticalAlignment: VerticalAlignment.Bottom
        horizontalAlignment: HorizontalAlignment.Right
        //accessibility.name: "Edit Cover Art"
        
        onClicked: {
            editCoverArt()
        }
        
        attachedObjects: [
            SystemDialog {
                id: pictureLocationDialog
                title: qsTr("Cover Art Location")
                body: qsTr("If the Cover Art picture is already on your BlackBerry choose Local. Otherwise choose Search to look for the Cover Art on the internet.")
                confirmButton.label: qsTr("Local")
                cancelButton.label: qsTr("Search")
                onFinished: {
                	if(value == SystemUiResult.ConfirmButtonSelection){
                	    coverArtPicker.open()
                	}else{
                	    //invoke
                        coverArtSearch.search(artist, album); //coverArtContainer.map);
                	}
                }
            },
            CoverArtSearch {
                id: coverArtSearch
                onImagePathChanged: {
                    console.log("ImagePathChanged")
                    if(imagePath.length > 0){
                        coverArt.imageSource = "file://"+imagePath
                        newCoverArtURL = imagePath;
                        pictureEditor.doLaunch("fixedsize", newCoverArtURL, "300x300")   
                    }
                }
            },
            FilePicker {
                id: coverArtPicker
                type: FileType.Picture
                title: qsTr("Select Cover Art")
                filter: ["*.jpg","*.jpeg","*.png"]
                onFileSelected: {
                    console.log("FileSelected signal received : " + selectedFiles);
                    
                    //make sure to prepend "file://" when using as a source for an ImageView or MediaPlayer
                    coverArt.imageSource = "file://" + selectedFiles[0];
                    newCoverArtURL = selectedFiles[0];
                    pictureEditor.doLaunch("fixedsize", newCoverArtURL, "300x300")
                    
                }
            },
            PictureEditorLauncher{
                id: pictureEditor
                onPictureUrlChanged: {
                    console.log(url)
                    coverArt.imageSource = "file://" + url;
                    newCoverArtURL = url;
                }
            }
        ]
    }
    ImageButton {
        id: deleteImageButton
        //visible: false
        visible: editable && (coverArt.imageSource != "asset:///default_cover_art.png")
        defaultImageSource: "asset:///images/ic_delete.png"
        verticalAlignment: VerticalAlignment.Bottom
        horizontalAlignment: HorizontalAlignment.Left
        //accessibility.name: "Delete Cover Art"
        
        onClicked: {
            deleteCoverArtDialog.show()
        }
        attachedObjects: [
            SystemDialog {
                id: deleteCoverArtDialog
                body: qsTr("Delete the cover art for this song?")
                title: qsTr("Delete Cover Art")
                confirmButton.label: qsTr("OK")
                cancelButton.label: qsTr("Cancel")
                onFinished: {
                    if(value == SystemUiResult.ConfirmButtonSelection){
                        coverArt.imageSource = "asset:///images/default_cover_art.png"
                        newCoverArtURL = "";
                        deleted = true;
                    }
                }
            }
        ]
    }
}