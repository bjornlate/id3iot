import bb.cascades 1.0
import bb.system 1.0

Page {
    id: tagEditPage
    property variant tagMap
    property variant indexPath
    property string newCoverArtURL
    signal playSong(string fid, bool preview)
    signal save(variant indexPath, variant map)
    signal deleteFile(string filePath)
    
    attachedObjects: [
        ActionItem {
            id: editCoverArtAction
            title: qsTr("Edit Cover Art")
            imageSource: "asset:///images/ic_edit_profile.png"
            ActionBar.placement: ActionBarPlacement.InOverflow
            onTriggered: {
                coverArt.editCoverArt()
            }
        }
    ]
    
    onTagMapChanged: {
        if(tagMap.filename != undefined){
            var filename = tagMap.filename
            coverArt.editable = (filename.toLowerCase().indexOf(".mp3") > -1);
            if(coverArt.editable){
                tagEditPage.addAction(editCoverArtAction)
            }else{
                tagEditPage.removeAction(editCoverArtAction)
            }
        }
    }
    
    titleBar: TitleBar {
        title: qsTr("Edit ID3 Tag")
        acceptAction: ActionItem {
            title: qsTr("Save")
            onTriggered: {
                scrollView.visible = false
                scrollView.enabled = false;

                spinner.start();

                var map = tagMap;
                map.artist = _artist.text;
                map.album = _album.text;
                map.title = _title.text;
                map.comment = _comment.text;
                map.year = _year.text;
                map.track = _track.text;
                map.compilation = _compilation.checked ? 1 : 0;
                if (_genres.selectedIndex >= 0) {
                    map.genre = _genres.selectedOption.text
                }
                map.customGenre = _customGenre.text
                map.newCoverArtURL = coverArt.newCoverArtURL
                //This will update the single's cover art imate
                if(map.newCoverArtURL.length > 0){
                    map.thumbnail = map.newCoverArtURL;
                    map.coverart = map.newCoverArtURL
                }else if(coverArt.deleted){
                    map.thumbnail = "";
                    map.coverart = "";
                }
                map.deleteCoverArt = coverArt.deleted
                save(indexPath, map);
            }
        }
        dismissAction: ActionItem {
            title: qsTr("Cancel")
        }
    }

    actions: [
        ActionItem {
            title: qsTr("Play Song")
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/ic_play.png"
            onTriggered: {
                playSong(tagMap.filepath, true)
            }
        },
        ActionItem {
            id: musicPlayerAction
            title: qsTr("Open in Music")
            imageSource: "asset:///images/ic_music_library.png"
            onTriggered: {
                playSong(tagMap.fid, false)
            }
        },
        ActionItem {
            property bool singleMode: true
            id: albumTitleModeAction
            title: qsTr("Song as Album Name")           
            imageSource: "asset:///images/ic_word_count.png"
            onTriggered: {
                if(singleMode){
                    _album.text = _title.text
                    albumTitleModeAction.title = qsTr("Normal Album Name")
                }else{
                    _album.text = tagMap.album
                    albumTitleModeAction.title = qsTr("Song as Album Name")
                }
                singleMode = !singleMode
            }
        },
        DeleteActionItem {
            attachedObjects: [
                SystemDialog {
                    id: deleteConfirm
                    body: qsTr("Are you sure you want to delete %1?").arg(tagMap.filename)
                    title: qsTr("Delete File")
                    onFinished: {
                        if(result == SystemUiResult.ConfirmButtonSelection){
                            console.log("delete confirmed");
                            deleteFile(tagMap.filepath)
                        }
                        
                    }
                }
            ]
            onTriggered: {
                deleteConfirm.show();
            }
        }
    ]
    content: Container {
        layout: DockLayout {
        }

        ActivityIndicator {
            id: spinner
            preferredWidth: 400
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
        }

        ScrollView {
            id: scrollView
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Fill
            content: Container {
                topPadding: 20
                leftPadding: 20
                rightPadding: 20

                Label {
                    text: qsTr("File")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }
                Label {
                    id: fileName
                    text: tagMap.filename
                    multiline: true
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    CoverArt {
                        id: coverArt
                        artist: _artist.text
                        album: _album.text
                    }
                    Container {
                        verticalAlignment: VerticalAlignment.Center
                        leftMargin: 12

                        Label {
                            text: qsTr("Bitrate")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                            verticalAlignment: VerticalAlignment.Center
                        }
                        Label {
                            text: tagMap.bitrate + " kbps"
                            //textStyle.fontSize: FontSize.Small
                        }
                        Label {
                            text: qsTr("Sample Rate")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                            verticalAlignment: VerticalAlignment.Center
                        }
                        Label {
                            text: tagMap.sampleRate + " Hz"
                            //textStyle.fontSize: FontSize.Small
                        }
                    }
                }

                Divider {

                }
                Label {
                    text: qsTr("Title")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight

                    }

                    Container {
                        layout: StackLayout {
                        }

                        TextField {
                            id: _title
                            hintText: qsTr("Title")
                            text: tagMap.title
                        }

                        Label {
                            text: qsTr("Artist")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                        }
                        TextField {
                            id: _artist
                            hintText: qsTr("Artist")
                            text: tagMap.artist
                        }
                    }
                    Button {
                        maxWidth: 0
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            var artistStr = _artist.text
                            _artist.text = _title.text
                            _title.text = artistStr
                        }
                        imageSource: "asset:///images/ic_swap_arrow.png"
                        preferredWidth: 81
                    }
                }
                Label {
                    text: qsTr("Album")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }
                TextField {
                    id: _album
                    hintText: qsTr("Album")
                    text: tagMap.album
                }

                Container {
                    Container {
                        Label {
                            text: qsTr("Track")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                        }
                        TextField {
                            id: _track
                            hintText: qsTr("Track")
                            text: tagMap.track
                            inputMode: TextFieldInputMode.NumbersAndPunctuation
                            preferredWidth: 130
                            //maximumLength: 4
                        }
                        rightMargin: 30
                    }
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        Label {
                            text: qsTr("Year")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                        }
                        TextField {
                            id: _year
                            hintText: qsTr("Year")
                            text: tagMap.year
                            inputMode: TextFieldInputMode.NumbersAndPunctuation
                            preferredWidth: 160
                            //maximumLength: 4
                        }
                        rightMargin: 30

                    }
                    Container {
                        visible: false
                        Label {
                            text: qsTr("Disc")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                        }
                        TextField {
                            id: _disc
                            hintText: qsTr("Disc")
                            text: tagMap.disc
                            inputMode: TextFieldInputMode.NumbersAndPunctuation
                            preferredWidth: 130
                            //maximumLength: 4
                        }
                    }
                }
                CheckBox {
                    id: _compilation
                    text: qsTr("Compilation")
                    checked: tagMap.compilation != 0
                    verticalAlignment: VerticalAlignment.Top
                    visible: false
                }

                Divider {
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    DropDown {
                        id: _genres
                        title: qsTr("Genre")
                        objectName: "genres"
                        verticalAlignment: VerticalAlignment.Center

                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 5
                        }
                    }
                    TextField {
                        verticalAlignment: VerticalAlignment.Center
                        id: _customGenre
                        hintText: qsTr("Custom Genre")
                        objectName: "customGenre"
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 5
                        }
                        onTextChanged: {
                            _genres.enabled = text.length == 0
                        }
                    }
                }
                Label {
                    text: qsTr("Comment")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }
                TextArea {
                    id: _comment
                    editable: true
                    hintText: qsTr("Comment")
                    text: tagMap.comment
                    maximumLength: 120
                    preferredHeight: 160
                }
            }
        }
    }
    onCreationCompleted: {
        tagMap = {
            bitrate: 1234,
            sampleRate: 44440
        };

    }
}
