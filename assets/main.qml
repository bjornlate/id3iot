import bb.cascades 1.0
import bb.multimedia 1.0

Page {
    property bool showSorting: false;
    id: listPage
    onShowSortingChanged: {
        sortingOptions.visible = showSorting
        if(showSorting){
            listPage.addAction(hideSorting);
        }
    }
    actions: [
        MultiSelectActionItem {
            onTriggered: {
                listView.multiSelectHandler.active = true
            }
        }
    ]
    attachedObjects: [
        MediaPlayer {
            id: player
            sourceUrl: "asset:///eediot.mp3"

            onMediaStateChanged: {
                /*switch (player.mediaState) {
                 * case MediaState.Unprepared:
                 * break;
                 * case MediaState.Prepared:
                 * break;
                 * }*/
            }
        },
        ActionItem {
            id: hideSorting
            title: sortingOptions.visible ? qsTr("Hide Sort Options") : qsTr("Show Sort Options")
            imageSource: "asset:///images/ic_sort.png"
            onTriggered: {
                sortingOptions.visible = !sortingOptions.visible
            }
        }
    ]
    titleBar: TitleBar {
        kind: TitleBarKind.FreeForm
        appearance: TitleBarAppearance.Plain
        kindProperties: FreeFormTitleBarKindProperties {
            content: Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 12
                ImageButton {
                    defaultImageSource: "asset:///images/derp.png"

                    preferredWidth: 54 * 1.3
                    preferredHeight: 57 * 1.3
                    verticalAlignment: VerticalAlignment.Center
                    onClicked: {
                        if (player.play() != MediaError.None) {
                            console.log("MediaError :/");
                        }
                    }
                }
                Label {
                    leftMargin: 4
                    text: "ID3IOT"
                    textStyle.base: SystemDefaults.TextStyles.BigText
                    verticalAlignment: VerticalAlignment.Center

                }
                verticalAlignment: VerticalAlignment.Center
                background: Color.White
            }
        }
    }
    Container {
        //Todo: fill me with QML
        Container {
            id: sortingOptions
            visible: false
            topPadding: 12
            bottomPadding: 12
            horizontalAlignment: HorizontalAlignment.Fill            
            SegmentedControl {
                options: [
                    Option {
                        text: qsTr("By Album")
                        onSelectedChanged: {
                            if(selected){
                                listView.dataModel.sortingKeys = ["album", "track"]
                            }
                        }
                    },
                    Option {
                        text: qsTr("By Genre")
                        onSelectedChanged: {
                            if(selected){
                                listView.dataModel.sortingKeys = ["genre", "album", "track"]
                            }
                        }
                    },
                    Option {
                        text: qsTr("By Artist")
                        onSelectedChanged: {
                            if(selected){
                            	listView.dataModel.sortingKeys = ["artist", "album", "track"]
                            }
                        }
                    }
                ]
            }
        }
        Container {
            layout: DockLayout {
            }
            Container {
                visible: listView.dataModel != undefined ? listView.dataModel.size == 0 : false
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                preferredWidth: 500
                Label {
                    horizontalAlignment: HorizontalAlignment.Center

                    text: qsTr("There are no songs to display in this list")
                    textStyle.fontSize: FontSize.XLarge
                    textStyle.fontWeight: FontWeight.W300
                    textStyle.textAlign: TextAlign.Center
                    multiline: true
                }
            }
            ListView {
                id: listView
                objectName: "listView"

                layout: StackListLayout {
                    headerMode: ListHeaderMode.Sticky
                }

                signal playSong(string fid, bool preview)
                signal editMultiple(variant indexPaths)
                listItemComponents: [
                    ListItemComponent {
                        type: "header"
                        Header {
                            title: ListItemData.length == 0 ? qsTr("Unknown") : ListItemData
                        }
                    },
                    ListItemComponent {
                        type: "item"
                        StandardListItem {
                            id: trackItem
                            title: ListItemData.title
                            description: ListItemData.artist
                            status: ListItemData.track
                            imageSource: (ListItemData.coverart != undefined && ListItemData.coverart.length > 0) ? "file://" + ListItemData.thumbnail : "asset:///images/default_cover_art.png"

                            contextActions: ActionSet {
                                title: trackItem.ListItem.data.title
                                subtitle: trackItem.ListItem.data.artist
                                ActionItem {
                                    id: playAction
                                    title: qsTr("Play Song")
                                    imageSource: "asset:///images/ic_play.png"
                                    onTriggered: {
                                        var filePath = trackItem.ListItem.data.mountpath
                                        filePath += trackItem.ListItem.data.basepath
                                        filePath += trackItem.ListItem.data.filename
                                        trackItem.ListItem.view.playSong(filePath, true)
                                    }
                                }
                                ActionItem {
                                    id: musicPlayerAction
                                    title: qsTr("Open in Music")
                                    imageSource: "asset:///images/ic_music_library.png"
                                    onTriggered: {
                                        trackItem.ListItem.view.playSong(trackItem.ListItem.data.fid, false)
                                    }
                                }
                            }
                        }
                    }
                ]
                multiSelectAction: MultiSelectActionItem {
                }

                multiSelectHandler {
                    actions: [
                        ActionItem {
                            title: qsTr("Edit Tags")
                            imageSource: "asset:///images/ic_edit.png"
                            onTriggered: {
                                listView.editMultiple(listView.selectionList())
                            }
                        }
                    ]
                }
            }
        }
        Divider {
            topMargin: 0
        }
        Container {
            bottomPadding: 12
            leftPadding: 12
            rightPadding: 12
            TextField {
                id: searchField
                objectName: "searchField"
                hintText: qsTr("Search for Song, Artist, Album or Genre name")
            }
        }
    }
}
