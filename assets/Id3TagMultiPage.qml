import bb.cascades 1.0

Page {
    id: tagEditPage
    property variant tagMap;
    property variant filePathToIndexPath;

    signal playSong(string fid)
    signal save(variant indexPath, variant map)
    
    property alias coverArtEditable: coverArt.editable
    
    attachedObjects: [
        ActionItem {
            id: editCoverArtAction
            title: qsTr("Edit Cover Art")
            imageSource: "asset:///images/ic_edit_profile.png"
            ActionBar.placement: ActionBarPlacement.InOverflow
            onTriggered: {
                coverArt.editCoverArt()
            }
        }
    ]
    
    onTagMapChanged: {
        console.log("Paul doesn't work weekends")
        if(tagMap.extension != undefined){
            coverArtEditable = tagMap.extension.toLowerCase() == "mp3";
            console.log("CoverArtEditable? " + coverArtEditable);
            if(coverArtEditable){
                tagEditPage.addAction(editCoverArtAction)
            }else{
                tagEditPage.removeAction(editCoverArtAction)
            }
        }
    }
    
    titleBar: TitleBar {
        title: qsTr("Edit ID3 Tags")
        acceptAction: ActionItem {
            title: qsTr("Save")
            onTriggered: {
                scrollView.visible = false
                scrollView.enabled = false;
                spinner.start();

                var map = {};
                if (_artist.enabled) {
                    map["artist"] = _artist.text;
                }
                if (_album.enabled) {
                    map["album"] = _album.text;
                }
                if (_comment.enabled) {
                    map["comment"] = _comment.text;
                }
                if (_year.enabled) {
                    map["year"] = _year.text;
                }
                if (_genres.enabled) {
                    if(_genres.selectedOption != null){
                    	map["genre"] = _genres.selectedOption.text
                    }
                    map["customGenre"] = _customGenre.text
                }
                map.newCoverArtURL = coverArt.newCoverArtURL
                map.deleteCoverArt = coverArt.deleted

                //map.compilation = _compilation.checked;
                save(filePathToIndexPath, map);
            }
        }
        dismissAction: ActionItem {
            title: qsTr("Cancel")
        }
    }

    content: Container {
        layout: DockLayout {
        }

        ActivityIndicator {
            id: spinner
            preferredWidth: 400
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
        }

        ScrollView {
            id: scrollView
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Fill

            content: Container {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    CheckBox {
                        id: coverArtCheckBox
                        visible: false
                    }
                    CoverArt {
                        id: coverArt
                    }
                }
                topPadding: 20
                leftPadding: 20
                rightPadding: 20

                Label {
                    text: qsTr("Artist")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }
                Container {
                    CheckBox {
                        id: artistCheckBox
                        verticalAlignment: VerticalAlignment.Center
                        checked: false
                    }
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    TextField {
                        id: _artist
                        hintText: qsTr("Artist")
                        text: tagMap.artist
                        enabled: artistCheckBox.checked
                    }
                }

                Label {
                    text: qsTr("Album")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }
                Container {
                    CheckBox {
                        id: albumCheckBox
                        verticalAlignment: VerticalAlignment.Center
                        checked: false
                    }
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    TextField {
                        id: _album
                        hintText: qsTr("Album")
                        text: tagMap.album
                        enabled: albumCheckBox.checked
                    }
                }

                Container {

                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    Container {
                        Label {
                            text: qsTr("Year")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                        }
                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            CheckBox {
                                id: yearCheckBox
                                verticalAlignment: VerticalAlignment.Center
                                checked: false
                            }
                            TextField {
                                id: _year
                                hintText: qsTr("Year")
                                text: tagMap.year
                                inputMode: TextFieldInputMode.NumbersAndPunctuation
                                preferredWidth: 160
                                //maximumLength: 4
                                enabled: yearCheckBox.checked
                            }
                        }
                        rightMargin: 30

                    }
                    Container {
                        visible: false
                        Label {
                            text: qsTr("Disc")
                            textStyle.fontSize: FontSize.XSmall
                            textStyle.fontStyle: FontStyle.Italic
                        }
                        TextField {
                            id: _disc
                            hintText: qsTr("Disc")
                            text: tagMap.disc
                            inputMode: TextFieldInputMode.NumbersAndPunctuation
                            preferredWidth: 130
                            //maximumLength: 4
                        }
                    }
                    CheckBox {
                        id: _compilation
                        text: qsTr("Compilation")
                        checked: tagMap.compilation
                        verticalAlignment: VerticalAlignment.Top
                        visible: false
                    }
                }

                Divider {
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    CheckBox {
                        verticalAlignment: VerticalAlignment.Center
                        id: genreCheckBox
                        checked: false
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        DropDown {
                            id: _genres
                            title: qsTr("Genre")
                            objectName: "genres"

                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 5
                            }
                        }
                        TextField {
                            id: _customGenre
                            hintText: qsTr("Custom Genre")
                            objectName: "customGenre"
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 5
                            }
                            onTextChanged: {
                                _genres.enabled = text.length == 0
                            }
                        }
                    }
                }
                Label {
                    text: qsTr("Comment")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    CheckBox {
                        id: commentCheckBox
                        verticalAlignment: VerticalAlignment.Top
                        checked: false
                    }
                    TextArea {
                        enabled: commentCheckBox.checked
                        id: _comment
                        editable: true
                        hintText: qsTr("Comment")
                        text: tagMap.comment
                        maximumLength: 120
                        preferredHeight: 160
                    }
                }
                Divider {
                }
                Label {
                    text: qsTr("Files")
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.fontStyle: FontStyle.Italic
                }

                Container {
                    objectName: "files"
                }
            }
        }
    }

    onCreationCompleted: {
        tagMap = {
            "fileName": "a/test/file/name/track/mp3"
        }
    }
}
