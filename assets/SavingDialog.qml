import bb.cascades 1.0

Container {
    verticalAlignment: VerticalAlignment.Fill
    horizontalAlignment: HorizontalAlignment.Fill
    layout: DockLayout {

    }
    attachedObjects: [
        ImagePaintDefinition {
            imageSource: "asset:///images/listview_bg.png"
            id: gradient
        }
    ]
    background: gradient.imagePaint
    Container {
        bottomPadding: 120
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Bottom
        ActivityIndicator {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            id: spinner
            preferredWidth: 100
            rightMargin: 12
        }
        Label {
            leftMargin: 0
            text: qsTr("Updating tags...")
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            textStyle.fontSize: FontSize.XLarge
        }
    }
    onCreationCompleted: {
        spinner.start();
    }
    
}
